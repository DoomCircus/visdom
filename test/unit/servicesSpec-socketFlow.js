'use strict';
describe('Core services', function() {
  beforeEach(module('visdomNG'));

  describe('socketFlow', function() {
    //TODO: create mocker for VISDOM that can be used elsewhere? or maybe create an angular service that instantiates a visdom to reference?
    var VISDOM = null;
    var mySocketFlow, global, my = {};

    var mysocket = {
      readyState: 1,
      send: function(msg) {
        console.log("Sending: " + msg);
      }
    };

    var myWebSocket = function(url) {
      return mysocket;
    };

    var rootScope, doUpdateFlow, doUpdateFlush;
    beforeEach(function() {
      global = jasmine.getGlobal();

      spyOn(global, 'WebSocket').andCallFake(myWebSocket);
      spyOn(mysocket, 'send');

    });
    beforeEach(inject(function($rootScope, socketFlow) {
      rootScope = $rootScope;
      mySocketFlow = socketFlow;
      doUpdateFlow = function(data) {
        var updata = angular.copy(data);
        mySocketFlow.updateData(updata);
        var parms, id;

        mysocket.send.calls.forEach(function(elm, ind, arr) {
          parms = elm.args[0].split(" ");
          id = parms[1];
          // console.log("handling response for " + parms);
          //TODO: mock service for this?
          if (parms[0] === "GET_GEO_LOCATION") {
            mySocketFlow.handleDeferResponse(id, [{
                "host": parms[2],
                "location": {
                  "lat": "37.419201",
                  "long": "-122.057404"
                },
                fromMock: true
              }
            ]);
          }
          if (parms[0] === "GET_ENTITY") {
            mySocketFlow.handleDeferResponse(id, [{
                "id": parms[2],
                "name": parms[2],
                "fromMock": true
              }
            ]);
          }
        });
      };
      doUpdateFlush = function() {
        //NOTE: flush timeouts because our stuff isn't hooked up to Mimir yet
        inject(function($injector) {
          var $timeout = $injector.get('$timeout');
          $timeout.flush();
        });

        rootScope.$apply();
      }
    }));

    it('should provide reference to the flow data', function() {
      var data = mySocketFlow.$get;
      expect(data).toBeDefined();
    });
    it('should create a new websocket', function() {
      expect(global.WebSocket).toHaveBeenCalled();
      // console.log(global.WebSocket.mostRecentCall.args);
    });
    it('should define callback functions for socket', function() {
      expect(mysocket.onopen).toBeDefined();
      expect(mysocket.onmessage).toBeDefined();
      expect(mysocket.onclose).toBeDefined();
    });
    it('should send "START_FLOW_UPDATE" on .startUpdate', function() {
      mySocketFlow.startUpdate();
      expect(mysocket.send).toHaveBeenCalledWith("START_FLOW_UPDATE");
    });
    it('should call updateData() on receiving "flowUpdate" message type', function() {
      spyOn(mySocketFlow, 'updateData');
      var ev = {
        data: '{"type":"flowUpdate","id":"42","data":[{ "flowID": "10", "info": { "src": { "ip": "10.0.0.11", "port": "56439" }, "dst": { "ip": "74.125.134.125", "port": "5222" }, "sendbytes": "80543", "recvbytes": "129263" } }]}'
      }
      mysocket.onmessage(ev);
      expect(mySocketFlow.updateData).toHaveBeenCalled();
    });
    describe('updateData', function() {
      it('should update the send and recv bytes when a flow is updated', function() {
        doUpdateFlow(newdata);

        var flows = mySocketFlow.$get();
        var flow = flows[newdata[0].flowID];


        expect(flow.sendbytes).toEqual(newdata[0].info.sendbytes);
        expect(flow.recvbytes).toEqual(newdata[0].info.recvbytes);

        doUpdateFlow(newdata2);

        expect(flow.sendbytes).toEqual(newdata2[0].info.sendbytes);
        expect(flow.recvbytes).toEqual(newdata2[0].info.recvbytes);

      });
      it('should append default entity init data on new flow', function() {
        doUpdateFlow(newdata);
        var flows = mySocketFlow.$get();
        var flow = flows[newdata[0].flowID];
        expect(flow.src.ent).toEqual({
          id: '24.4.40.165',
          name: '24.4.40.165'
        });
        expect(flow.src.ent.flows.length).toBe(1); //flows is non enumerable
      });
      it('should emit an update flow event on flow completion', function() {
        var callback = jasmine.createSpy('flowEvent');
        rootScope.$on('flowUpdate', callback);
        doUpdateFlow(newdata);
        doUpdateFlush();
        doUpdateFlow(newdata2);
        expect(callback).toHaveBeenCalled();
      });
      it('should emit a create flow event on a new flow', function() {
        var callback = jasmine.createSpy('flowEvent');
        rootScope.$on('flowCreate', callback);
        doUpdateFlow(newdata);
        doUpdateFlush();
        expect(callback).toHaveBeenCalled();
      });
      it('should emit an delete flow event on flow expiration', function() {
        var callback = jasmine.createSpy('flowEvent');
        rootScope.$on('flowDestroy', callback);
        doUpdateFlow(newdata);
        doUpdateFlush();
        doUpdateFlow(newdata4);
        doUpdateFlush();
        expect(callback).toHaveBeenCalled();
      });
      describe('entities', function() {
        var flow, entities, flows;
        describe('new flows', function() {
          beforeEach(function() {
            spyOn(mySocketFlow, 'queryEntity').andCallThrough();

            doUpdateFlow(newdata);
            doUpdateFlush();

            flow = mySocketFlow.$get()[13];
            entities = mySocketFlow.$getEnts();
            flows = mySocketFlow.$get();
          });
          it('should push the flowid to each entity', function() {
            expect(flow).toBeDefined();
            expect(flow.src.ent.flows.length).toBe(1);
            expect(flow.dst.ent.flows.length).toBe(1);
          });
          it('should create two entities from a brand new flow', function() {
            expect(Object.keys(entities).length).toBe(2);
          });
          it('should have three entities from two flows that share an entity', function() {
            expect(Object.keys(entities).length).toBe(2);
            doUpdateFlow(newdata3);
            // console.log(mySocketFlow.$get());

            doUpdateFlush();
            expect(Object.keys(entities).length).toBe(3);
            expect(flow.src.ent.flows.length).toBe(2);
          });
          it('should resolve two entities on a new flow', function() {
            expect(mySocketFlow.queryEntity.calls.length).toBe(2);
          })
          it('should only resolve three entities given two ent-sharing flows', function() {
            doUpdateFlow(newdata3);
            expect(mySocketFlow.queryEntity.calls.length).toBe(3);
          });
          it('should delete flows and empty entities when destroyFlow is called with a single flow in the model', function() {
            mySocketFlow.destroyFlow(13);
            expect(Object.keys(entities).length).toBe(0);
            expect(flow.dst.ent.flows.length).toBe(0);
            expect(flow.src.ent.flows.length).toBe(0);
            expect(flows[13]).not.toBeDefined();
            expect(entities[flow.dst.ent.id]).not.toBeDefined();
            expect(entities[flow.src.ent.id]).not.toBeDefined();
          });
          it('should delete flows and empty entities when destroyFlow is called with a two ent-sharing flows in the model', function() {
            doUpdateFlow(newdata3);
            doUpdateFlush();
            expect(Object.keys(entities).length).toBe(3);
            mySocketFlow.destroyFlow(13);
            expect(Object.keys(entities).length).toBe(2);
            expect(flow.dst.ent.flows.length).toBe(0);
            expect(flow.src.ent.flows.length).toBe(1);
            expect(flows[13]).not.toBeDefined();
          });
          it('should broadcast an event when a flow is deleted', function() {
            var callback = jasmine.createSpy('flowEvent');
            rootScope.$on('flowDestroy', callback);
            mySocketFlow.destroyFlow(13);
            expect(callback).toHaveBeenCalled();
          })
          it('should append entity location data (as type number) when resolving a new entity', function() {
            expect(flow.src.ent.location).toEqual({
              "lat": 37.419201,
              "long": -122.057404
            });
            expect(flow.dst.ent.location).toEqual({
              "lat": 37.419201,
              "long": -122.057404
            });

          });
        });
        describe('flow update events', function() {
          it('should not crash when flow update requests happen again before promises are resolved', function() {
            doUpdateFlow(newdata3);
            flow = mySocketFlow.$get()[13];
            entities = mySocketFlow.$getEnts();
            flows = mySocketFlow.$get();
          });
        });
      });
    });
    describe('geoIP', function() {
      it('should call handleDeferResponse on receiving "geoipResponse" message type', function() {
        spyOn(mySocketFlow, 'handleDeferResponse');
        var ev = {
          data: '{"type":"geoipResponse","id":"42","data":[{"host":"8.8.8.8","location":{"lat":"37.419201","long":"-122.057404"}}]}'
        }
        mysocket.onmessage(ev);
        expect(mySocketFlow.handleDeferResponse).toHaveBeenCalled();
        // console.log(mySocketFlow.handleDeferResponse.mostRecentCall.args);
      });
      it('should call handleDeferResponse on receiving "geoipResponse" message type when private too', function() {
        spyOn(mySocketFlow, 'handleDeferResponse');
        var ev = {
          data: '{"type":"geoipResponse","id":"43","data":[]}'
        }
        mysocket.onmessage(ev);
        expect(mySocketFlow.handleDeferResponse).toHaveBeenCalled();
      });
      it('should query goflow for geolocation data when queryGeoIP is called and return a promise', function() {
        var promise = mySocketFlow.queryGeoIP("8.8.8.8");
        expect(mysocket.send).toHaveBeenCalled();
        // console.log(mysocket.send.mostRecentCall.args);
        expect(mysocket.send.mostRecentCall.args).toMatch(/GET_GEO_LOCATION [0-9]* 8\.8\.8\.8/);
        expect(promise).toBeDefined();
      });
      it('should handle a geoIPResponse by updating the promise', inject(function($rootScope) {
        var promise = mySocketFlow.queryGeoIP("8.8.8.8");
        var resolved = null;
        promise.then(function(dataz) {
          resolved = dataz;
        }, function(errorz) {
          resolved = errorz;
        });
        //get the chosen id
        var parms = mysocket.send.calls[0].args[0].split(" ");
        var id = parms[1];

        mySocketFlow.handleDeferResponse(id, [{
            "host": "8.8.8.8",
            "location": {
              "lat": "37.419201",
              "long": "-122.057404"
            }
          }
        ]);
        expect(resolved).toBe(null);

        $rootScope.$apply(); //required to resolve promise

        expect(resolved).toNotBe(null);
      }));
      it('should return an object {location: null} if the geoIP address is private', inject(function($rootScope) {
        var promise = mySocketFlow.queryGeoIP("10.0.0.5");
        var resolved = undefined;
        promise.then(function(dataz) {
          resolved = dataz;
        }, function(errorz) {
          resolved = errorz;
        });
        //get the chosen id
        var parms = mysocket.send.calls[0].args[0].split(" ");
        var id = parms[1];

        mySocketFlow.handleDeferResponse(id, []);
        expect(resolved).not.toBeDefined();

        $rootScope.$apply(); //required to resolve promise

        expect(resolved.location).toEqual(null);
      }));
    });
    describe('entities', function() {
      it('should call handleEntityResponse on receiving "entityResponse" message type', function() {
        spyOn(mySocketFlow, 'handleDeferResponse');
        var ev = {
          data: '{"type":"entityResponse","id":"42","data":[{"id":"8.8.8.8:53","name":"n8.8.8.8:53"}]}'
        }
        mysocket.onmessage(ev);
        expect(mySocketFlow.handleDeferResponse).toHaveBeenCalled();
        // console.log(mySocketFlow.handleDeferResponse.mostRecentCall.args);
      });
      //---------------------
      //NOTE: temporarily out until backend supports entity resolution
      //TODO: support entity resolution in backend
      /*it('should query goflow for entity data when queryEntity is called and return a promise', function() {
        var promise = mySocketFlow.queryEntity("8.8.8.8:53");
        expect(mysocket.send).toHaveBeenCalled();
        // console.log(mysocket.send.mostRecentCall.args);
        expect(mysocket.send.mostRecentCall.args).toMatch(/GET_ENTITY [0-9]* 8\.8\.8\.8:53/);
        expect(promise).toBeDefined();
      });
      it('should handle an entityResponse by updating the promise', inject(function($rootScope) {
        var promise = mySocketFlow.queryEntity("8.8.8.8:53");
        var resolved = null;
        promise.then(function(dataz) {
          resolved = dataz;
        }, function(errorz) {
          resolved = errorz;
        });
        //get the chosen id
        var parms = mysocket.send.calls[0].args[0].split(" ");
        var id = parms[1];

        mySocketFlow.handleEntityResponse(id, [{
            "id": "8.8.8.8:53",
            "name": "n8.8.8.8:53"
          }
        ]);
        expect(resolved).toBe(null);

        $rootScope.$apply(); //required to resolve promise

        expect(resolved).toNotBe(null);
        // console.log(resolved);
      }));*/
      //-----------------------
      it('should initialize new entities when calling fillEntityInfo on uknowns', function() {
        var ents = mySocketFlow.$getEnts();
        var flows = mySocketFlow.$get();
        expect(ents).toEqual({});
        flows[13] = {
          src: {
            ip: '8.8.8.8'
          },
          dst: {
            ip: '9.9.9.9'
          }
        };
        mySocketFlow.fillEntityInfo(13);
        expect(ents).not.toEqual({});
        // console.log(ents);
      });
    });
    describe('socketFlow sorting', function() {
      var sortarray;
      var objmap;
      var orderby;
      var flow13 = {
        id: 13,
        sendbytes: 400,
        recvbytes: 200,
        src: {
          ip: "10.0.0.13",
          flowcount: 4
        }
      };
      var flow14 = {
        id: 14,
        sendbytes: 200,
        recvbytes: 400,
        src: {
          ip: "10.0.0.14",
          flowcount: 2
        }
      };
      beforeEach(function() {
        orderby = mySocketFlow.getOrderby();
        objmap = {};
        objmap[13] = flow13;
        objmap[14] = flow14;
      });
      describe('orderop compare', function() {
        it('should compare two values to determine ascending order', function() {
          var compare = orderby.compare(flow13, flow14);
          expect(compare).toBe(-1);
          compare = orderby.compare(flow14, flow13);
          expect(compare).toBe(1);
          compare = orderby.compare(flow14, flow14);
          expect(compare).toBe(0);
        });
        it('should compare two values to determine descending order', function() {
          orderby.asc = false;
          var compare = orderby.compare(flow13, flow14);
          expect(compare).toBe(1);
          compare = orderby.compare(flow14, flow13);
          expect(compare).toBe(-1);
          compare = orderby.compare(flow14, flow14);
          expect(compare).toBe(0);
        });
        it('should return undefined when the path is busted', function() {
          orderby.path = "foobar";
          expect(orderby.compare(flow13, flow14)).not.toBeDefined();
        });
      });
      it('should set orderby ascending id by default', function() {
        expect(orderby.asc).toBe(true);
        expect(orderby.path).toBe("id");
      });
      describe('binaryInsert', function() {
        it('should binaryInsert into sorted array of flows', function() {
          var objarray = [flow13, flow14];
          mySocketFlow.binaryInsert(objarray, {
            id: 18
          }, orderby);
          expect(objarray[2]).toEqual({
            id: 18
          });
          mySocketFlow.binaryInsert(objarray, {
            id: 16
          }, orderby);
          expect(objarray[2]).toEqual({
            id: 16
          });
          mySocketFlow.binaryInsert(objarray, {
            id: 10
          }, orderby);
          expect(objarray[0]).toEqual({
            id: 10
          });
        });
        it('should return undefined when orderby.path is invalid', function() {
          orderby.path = "ffjdjfjdksj";
          var objarray = [flow13, flow14];
          var retval = mySocketFlow.binaryInsert(objarray, {
            id: 18
          }, orderby);
          expect(retval).not.toBeDefined();
        });
      });
    });
    describe('socketFlow filters', function() {
      var filters;
      var objmap;
      var filterarray;
      var filterOp;
      var flow13 = {
        id: 13,
        sendbytes: 400,
        recvbytes: 200,
        src: {
          ip: "10.0.0.13",
          port: 80,
          flowcount: 4
        }
      };
      var flow14 = {
        id: 14,
        sendbytes: 200,
        recvbytes: 400,
        src: {
          ip: "10.0.0.14",
          port: 53,
          flowcount: 2
        }
      };
      beforeEach(function() {
        filters = mySocketFlow.getFilters();
        filterOp = new mySocketFlow.filterOp("<", 100, "foo.bar");
        objmap = {};
        objmap[13] = flow13;
        objmap[14] = flow14;
      });
      describe('filters array', function() {
        it('should have zero by default', function() {
          expect(filters).toEqual([]);
        });
        it('should add when calling addFilter', function() {
          mySocketFlow.addFilter({
            "foo": "bar"
          });
          expect(filters.length).toBe(1);
        });
        it('should add/delete a filter when calling add/removeFilter', function() {
          mySocketFlow.addFilter(filterOp);
          mySocketFlow.addFilter(filterOp);
          mySocketFlow.addFilter(filterOp);
          expect(filters.length).toBe(3);
          mySocketFlow.removeFilter(1);
          expect(filters.length).toBe(2);
        });
        it('should reset all filters when calling clearFilters', function() {
          mySocketFlow.addFilter(filterOp);
          mySocketFlow.addFilter(filterOp);
          mySocketFlow.addFilter(filterOp);
          expect(filters.length).toBe(3);
          mySocketFlow.clearFilters();
          expect(filters.length).toBe(0);
        })
      });
      describe('filterOp object', function() {
        it('should define a constructor with init and test function', function() {
          var op = new mySocketFlow.filterOp('<', 100, "foo.bar");
          expect(op.op).toEqual("<");
          expect(op.val).toEqual(100);
          expect(op.path).toEqual("foo.bar");
          expect(op.test).toBeDefined();
        });
        it('should successfully compare < numbers', function() {
          var testobj = {
            foo: 75
          };
          var op = new mySocketFlow.filterOp('<', 100, "foo");
          expect(op.test(testobj)).toBeTruthy();
          testobj.foo = 150;
          expect(op.test(testobj)).toBeFalsy();
        });
        it('should successfully compare > numbers', function() {
          var testobj = {
            foo: 150
          };
          var op = new mySocketFlow.filterOp('>', 100, "foo");
          expect(op.test(testobj)).toBeTruthy();
          testobj.foo = 75;
          expect(op.test(testobj)).toBeFalsy();
        });
        it('should successfully compare strings', function() {
          var testobj = {
            foo: "foo is bar"
          };
          var op = new mySocketFlow.filterOp('=', "bar", "foo");
          expect(op.test(testobj)).toBeTruthy();
          testobj.foo = "foo is lololol";
          expect(op.test(testobj)).toBeFalsy();
        });
        it('should successfully compare = numbers', function() {
          var testobj = {
            foo: 150
          };
          var op = new mySocketFlow.filterOp('=', 150, "foo");
          expect(op.test(testobj)).toBeTruthy();
          testobj.foo = 100;
          expect(op.test(testobj)).toBeFalsy();
        });
        it('should successfully compare ! operand', function() {
          var testobj = {
            foo: 150
          };
          var op = new mySocketFlow.filterOp('!', 150, "foo");
          expect(op.test(testobj)).toBeFalsy();
          testobj.foo = 100;
          expect(op.test(testobj)).toBeTruthy();
        });
        describe('evalPath', function() {
          var op, prop;
          beforeEach(function() {
            op = new mySocketFlow.filterOp("<", 100, "foo");
          });
          it('should evaluate a path one deep', function() {
            op.path = "sendbytes";
            prop = mySocketFlow.evalPath(op.path, flow13);
            expect(prop).toBeDefined();
          });
          it('should evaluate nested object paths', function() {
            op.path = "src.flowcount";
            prop = mySocketFlow.evalPath(op.path, flow13);
            expect(prop).toBeDefined();
          });
          it('should return undefined for a one-deep-invalid path', function() {
            op.path = "foobar";
            prop = mySocketFlow.evalPath(op.path, flow13);
            expect(prop).not.toBeDefined();
          });
          it('should return undefined for a multiple-deep-invalid path', function() {
            op.path = "foo.bar"
            prop = mySocketFlow.evalPath(op.path, flow13);
            expect(prop).not.toBeDefined();
          });
        });
      });
      describe('checkFilter object compare', function() {
        it('should return true (passes filter) when filters is empty', function() {
          filterarray = [];
          var retval = mySocketFlow.checkFilter(objmap[13], filterarray);
          expect(retval).toBeTruthy();
        });
        it('should return false when filterarray contains a filterOp that the obj fails', function() {
          filterarray = [
            new mySocketFlow.filterOp('>', 900, "sendbytes")
          ];
          var retval = mySocketFlow.checkFilter(objmap[13], filterarray);
          expect(retval).toBeFalsy();
        });
        it('should return true when filterarray contains a filterOp that the obj passes', function() {
          filterarray = [
            new mySocketFlow.filterOp('>', 100, "sendbytes")
          ];
          var retval = mySocketFlow.checkFilter(objmap[13], filterarray);
          expect(retval).toBeTruthy();
        });
        it('should return true when filterarray contains a nestedfilterOp that the obj passes', function() {
          filterarray = [
            new mySocketFlow.filterOp('>', 3, "src.flowcount")
          ];
          var retval = mySocketFlow.checkFilter(objmap[13], filterarray);
          expect(retval).toBeTruthy();
        });
        it('should return false when filterarray contains a nestedfilterOp that the obj fails', function() {
          filterarray = [
            new mySocketFlow.filterOp('>', 9, "src.flowcount")
          ];
          var retval = mySocketFlow.checkFilter(objmap[13], filterarray);
          expect(retval).toBeFalsy();
        });
        it('should treat a single OR filter like an AND thus only return matching', function() {
          filterarray = [
            new mySocketFlow.filterOp('>', 500, "sendbytes", true)
          ];
          var retval = mySocketFlow.checkFilter(objmap[13], filterarray);
          expect(retval).toBeFalsy();
        });
        it('should support multiple filters ANDed', function() {
          filterarray = [
            new mySocketFlow.filterOp('<', 500, "sendbytes"),
            new mySocketFlow.filterOp('>', 3, "src.flowcount")
          ];
          var retval = mySocketFlow.checkFilter(objmap[13], filterarray);
          expect(retval).toBeTruthy();

          filterarray[1] = new mySocketFlow.filterOp('<', 3, "src.flowcount");
          retval = mySocketFlow.checkFilter(objmap[13], filterarray);
          expect(retval).toBeFalsy();
        });
        it('should support multiple filters ORed', function() {
          filterarray = [
            new mySocketFlow.filterOp('>', 500, "sendbytes"),
            new mySocketFlow.filterOp('>', 3, "src.flowcount", true)
          ];
          var retval = mySocketFlow.checkFilter(objmap[13], filterarray);
          expect(retval).toBeTruthy();

          filterarray[1] = new mySocketFlow.filterOp('<', 3, "src.flowcount", true);
          retval = mySocketFlow.checkFilter(objmap[13], filterarray);
          expect(retval).toBeFalsy();

        });
        it('should support filter groups', function() {
          filterarray = [{
              "filterOps": [
                new mySocketFlow.filterOp('<', 500, "sendbytes"),
                new mySocketFlow.filterOp('>', 300, "sendbytes")
              ]
            }
          ];
          var retval = mySocketFlow.checkFilter(objmap[13], filterarray);
          expect(retval).toBeTruthy();

          filterarray[0].filterOps[0] = new mySocketFlow.filterOp('<', 200, "sendbytes");
          retval = mySocketFlow.checkFilter(objmap[13], filterarray);
          expect(retval).toBeFalsy();
        });
        it('should support filter groups ANDed', function() {
          filterarray = [{
              "filterOps": [
                new mySocketFlow.filterOp('<', 500, "sendbytes"),
                new mySocketFlow.filterOp('>', 300, "sendbytes")
              ]
            },
            new mySocketFlow.filterOp('>', 3, "src.flowcount")
          ];
          var retval = mySocketFlow.checkFilter(objmap[13], filterarray);
          expect(retval).toBeTruthy();

          filterarray[0].filterOps[0] = new mySocketFlow.filterOp('<', 200, "sendbytes");
          retval = mySocketFlow.checkFilter(objmap[13], filterarray);
          expect(retval).toBeFalsy();
        });
        it('should support filter groups ORed', function() {
          filterarray = [{
              "logicalOr": true,
              "filterOps": [
                new mySocketFlow.filterOp('<', 500, "sendbytes"),
                new mySocketFlow.filterOp('>', 300, "sendbytes")
              ]
            },
            new mySocketFlow.filterOp('<', 3, "src.flowcount")
          ];
          var retval = mySocketFlow.checkFilter(objmap[13], filterarray);
          expect(retval).toBeTruthy();

          filterarray[1] = new mySocketFlow.filterOp('>', 3, "src.flowcount");
          retval = mySocketFlow.checkFilter(objmap[13], filterarray);
          expect(retval).toBeTruthy();
        });
        it('should return false when filterarray has a property that objmap does not', function() {
          filterarray = [
            new mySocketFlow.filterOp('<', 200, "fjdksjfkdjk")
          ];
          var retval = mySocketFlow.checkFilter(objmap[13], filterarray);
          expect(retval).toBeFalsy();
        });
      });
      describe('applyFilter', function() {
        it('should return finalarr argument or a new array', function() {
          // var insertspy = spyOn(mySocketFlow, "binaryInsert");

          var arr = [];
          var retval = mySocketFlow.applyFilter({}, [], arr);
          expect(retval).toBe(arr);
          retval = mySocketFlow.applyFilter({}, []);
          expect(retval).toEqual([]);
        });
        it('should filter out objmap properties according to a filterarray', function() {
          var arr = [];
          filterarray = [];
          filterarray[0] = new mySocketFlow.filterOp('>', 300, 'sendbytes');
          mySocketFlow.applyFilter(objmap, filterarray, arr);
          expect(arr).toEqual([flow13]);
        });
        it('should return all flow IDs when filterarray is empty', function() {
          var arr = [];
          filterarray = [];
          mySocketFlow.applyFilter(objmap, filterarray, arr);
          expect(arr).toEqual([flow13, flow14]);
        })
      });
      describe('generateKeysMap', function() {
        it('should generate an object containing properties for every property in src object', function() {
          var obj = {
            foo: 1,
            bar: function() {
              return true;
            },
            arr: [1, 2, 3],
            subobj: {
              foo: 'bar'
            }
          };
          var sumobj = {};
          mySocketFlow.generateKeysMap(obj, sumobj);
          expect(sumobj).toEqual({
            foo: true,
            bar: true,
            arr: true,
            subobj: {
              foo: true
            }
          });
        });
      });
    });
  });
});