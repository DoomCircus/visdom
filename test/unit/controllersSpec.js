'use strict';

/* jasmine specs for controllers go here */


describe('versionCtrl', function() {
  var ver;
  beforeEach(module('visdomNG'));

  beforeEach(inject(function($rootScope, $controller, version) {
    var scope = $rootScope.$new();
    var myCtrl = $controller('versionCtrl', {
      $scope: scope
    });
    ver = version;
  }));

  it('should show the version information', function() {
    expect(ver).toBeDefined();
  });
});

describe('navCtrl', function() {
  var scope;
  var navCtrl;
  var navServiceMock;
  beforeEach(module('visdomNG'));

  beforeEach(inject(function($rootScope, $controller) {
    scope = $rootScope.$new();
    navServiceMock = {
      $get: jasmine.createSpy("$get").andReturn([])
    };
    navCtrl = $controller('navCtrl', {
      $scope: scope,
      navService: navServiceMock
    });
  }));

  it('should provide the navService navItems list to scope', function() {
    expect(scope.navItems).toBeDefined();
    expect(navServiceMock.$get).toHaveBeenCalled();
  });
});

describe('notificationCtrl', function() {
  var scope;
  var noteCtrl;
  var notifyMock;
  var whitelistMock;
  beforeEach(module('visdomNG'));

  beforeEach(inject(function($rootScope, $controller) {
    scope = $rootScope.$new();
    notifyMock = jasmine.createSpyObj("notify", ['$get', 'acknowledge']);
    whitelistMock = jasmine.createSpyObj('whitelist', ['fingerprint']);

    noteCtrl = $controller('notificationCtrl', {
      $scope: scope,
      notify: notifyMock,
      whitelist: whitelistMock
    });
  }));

  it('should call into notify on ack and fingerprint', function() {
    scope.fingerprint();
    expect(whitelistMock.fingerprint).toHaveBeenCalled();
    scope.ack();
    expect(notifyMock.acknowledge).toHaveBeenCalled();
  });
});