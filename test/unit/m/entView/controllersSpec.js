'use strict';
describe('entDetailCtrl', function() {
  var scope, ctrl, callback, mysocketFlow;

  beforeEach(module('visdomNG.entView'));

  beforeEach(function() {
    mysocketFlow = jasmine.createSpyObj("mysocketFlow", ['$getEnts']);
    mysocketFlow.$getEnts.andReturn({
      "10.0.0.1": {
        foo: "bar"
      }
    });
  });

  beforeEach(inject(function($rootScope, $routeParams, $controller) {
    $routeParams.entID = '10.0.0.1';
    scope = $rootScope.$new();
    callback = jasmine.createSpy("entityFocused");
    scope.$on('entityFocused', callback);

    ctrl = $controller('entDetailCtrl', {
      $scope: scope,
      socketFlow: mysocketFlow
    });
  }));

  it('should set $scope.entID to routeParams', function() {
    expect(scope.entID).toBe('10.0.0.1');
  });

  it('should set $scope.entity to the resolved object', function() {
    expect(mysocketFlow.$getEnts).toHaveBeenCalled();
    expect(scope.entity).toBeDefined();
  });

  it('should dispatch an entityFocused event to rootScope for vis3d', function(){
    expect(callback).toHaveBeenCalled();
    expect(callback.mostRecentCall.args[1]).toBe("10.0.0.1");
  });
});