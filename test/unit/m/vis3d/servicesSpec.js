'use strict';

/* jasmine specs for services go here */

describe('vis3d services', function() {
	var flow = {
		"src": {
			"ip": "24.4.40.165",
			"port": "51624"
		},
		"dst": {
			"ip": "10.0.0.5",
			"port": "443"
		},
		"sendbytes": 69783,
		"recvbytes": 1000
	};
	beforeEach(function() {
		module('visdomNG.vis3d');
		module(function($provide) {
			$provide.value('socketFlow', {
				$get: jasmine.createSpy().andReturn(flow)
			});
		});
	});

	describe('vis3d', function() {
		var myvis3d;
		describe('instantiation', function() {
			afterEach(function() {
				myvis3d = undefined;
			});
			it('should instantiate the service', function() {
				expect(myvis3d).not.toBeDefined();
				inject(function(vis3d) {
					myvis3d = vis3d;
				});
				expect(myvis3d).toBeDefined();
			});
			it('should expose the start API for threejs initialization', function() {
				expect(myvis3d).not.toBeDefined();
				inject(function(vis3d) {
					myvis3d = vis3d;
				});
				expect(myvis3d.start).toBeDefined();
			});
		});
		describe('flow event handling', function(){
			beforeEach(function(){
				inject(function(vis3d){
					myvis3d = vis3d;
				});
			});
			it('should define a handleUpdate function for angular->3d interactions', function(){
				expect(myvis3d.handleUpdate).toBeDefined();
			});
		});
	});
});