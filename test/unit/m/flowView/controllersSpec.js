'use strict';

/* jasmine specs for controllers go here */

describe('flowGraphCtrl', function() {
  var scope, ctrl;
  var mysocketFlow = jasmine.createSpyObj('mysocketFlow', ['enableSmoothie', 'getFlows', 'toggleTimeSeries', 'getTimeSeries', 'getFlowKeys']);
  mysocketFlow.getTimeSeries.andReturn({});

  beforeEach(module('visdomNG.flowView'));

  beforeEach(inject(function($rootScope, $controller) {
    scope = $rootScope.$new();
    ctrl = $controller('flowGraphCtrl', {
      $scope: scope,
      socketFlow: mysocketFlow
    });
  }));

  it('should create "flows" model from socketFlow service getFlows', function() {
    expect(mysocketFlow.getFlows).toHaveBeenCalled();
  });

  it('should turn on time series tracking for smoothies', function() {
    expect(mysocketFlow.toggleTimeSeries).toHaveBeenCalled();
  });
  it('should set the averagePacket value for smoothie', function() {
    expect(mysocketFlow.getTimeSeries).toHaveBeenCalled();
  });

});

describe('flowDetailCtrl', function() {
  var scope, ctrl, callback, mysocketFlow;

  beforeEach(module('visdomNG.flowView'));

  beforeEach(function() {
    mysocketFlow = jasmine.createSpyObj("mysocketFlow", ['$get']);
    mysocketFlow.$get.andReturn({
      3: {
        foo: "bar"
      }
    });
  });

  beforeEach(inject(function($rootScope, $routeParams, $controller) {
    $routeParams.flowID = '3';
    scope = $rootScope.$new();
    callback = jasmine.createSpy("flowFocused");
    scope.$on('flowFocused', callback);

    ctrl = $controller('flowDetailCtrl', {
      $scope: scope,
      socketFlow: mysocketFlow
    });
  }));

  it('should set $scope.flowID to routeParams', function() {
    expect(scope.flowID).toBe('3');
  });

  it('should set $scope.flow to the resolved object', function() {
    expect(mysocketFlow.$get).toHaveBeenCalled();
    expect(scope.flow).toBeDefined();
  });

  it('should dispatch an flowFocused event to rootScope for vis3d', function() {
    expect(callback).toHaveBeenCalled();
    expect(callback.mostRecentCall.args[1]).toBe("3");
  });
});

describe('flowViewCtrl', function() {
  'use strict';
  var scope, ctrl;
  var mysocketFlow = jasmine.createSpyObj('mysocketFlow', ['getFlows', 'getFlowKeys', 'getFilters', 'addFilter', 'filterOp', 'clearFilter', 'getOrderby']);
  mysocketFlow.getFlows.andReturn(newdata3);
  mysocketFlow.getFlowKeys.andReturn({
    foo: true
  });

  beforeEach(module('visdomNG.flowView'));

  beforeEach(inject(function($rootScope, $controller) {
    scope = $rootScope.$new();
    ctrl = $controller('flowViewCtrl', {
      $scope: scope,
      socketFlow: mysocketFlow
    });
  }));

  it('should create flows, filters, and flowKeys scope', function() {
    expect(mysocketFlow.getFlows).toHaveBeenCalled();
    expect(mysocketFlow.getFilters).toHaveBeenCalled();
  });
});

describe('flowFilterCtrl', function() {
  'use strict';
  var scope, ctrl;
  var mysocketFlow = jasmine.createSpyObj('mysocketFlow', ['getFlowKeys', 'getFilters', 'addFilter', 'filterOp', 'clearFilter']);
  mysocketFlow.getFlowKeys.andReturn({
    foo: true
  });
  var mydialog = jasmine.createSpyObj('dialog', ['close']);
  beforeEach(module('visdomNG.flowView'));

  beforeEach(inject(function($rootScope, $controller) {
    scope = $rootScope.$new();
    ctrl = $controller('flowFilterCtrl', {
      $scope: scope,
      socketFlow: mysocketFlow,
      dialog: mydialog
    });
  }));

  it('should create flowKeys and filters on the scope', function() {
    expect(mysocketFlow.getFlowKeys).toHaveBeenCalled();
    expect(mysocketFlow.getFilters).toHaveBeenCalled();
  });

  it('should check for complete data before calling addFilter', function() {
    scope.currentOp = "";
    scope.currentCompVal = "";
    scope.currentPath = "foobar.lol";
    scope.createCustomFilter();
    expect(mysocketFlow.addFilter).not.toHaveBeenCalled();
    expect(scope.filterProblem).toBe(true);
  });
  describe('type conversion', function() {
    beforeEach(function() {
      scope.currentOp = "<";
      scope.currentPath = "foo.bar";
    });
    it('should convert compval strings to numbers', function() {
      scope.currentCompVal = "999";
      scope.createCustomFilter();
      expect(mysocketFlow.filterOp.mostRecentCall.args[1]).toBe(999);
    });
    it('should keep weird numstrings as strings', function() {
      scope.currentCompVal = "999px";
      scope.createCustomFilter();
      expect(mysocketFlow.filterOp.mostRecentCall.args[1]).toBe("999px");
    });
  });

});