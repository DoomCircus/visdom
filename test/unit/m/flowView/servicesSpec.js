'use strict';

describe('flowView services', function() {
	beforeEach(module('visdomNG.flowView'));
	describe('commonFilters', function() {
		var sampleFilters = [
			[{
					"desc": "IPv4 source address",
					"modifiable": true,
					"userVal": "10.0.0.1",
					"filterOps": [{
							"op": "=",
							"path": "src.ip"
						}
					]
				}
			],
			[{
					"desc": "Web",
					"filterOps": [{
							"logicalOr": true,
							"filterOps": [{
									"op": "=",
									"val": 80,
									"path": "dst.port",
									"logicalOr": true
								}, {
									"op": "=",
									"path": "dst.port",
									"val": 443,
									"logicalOr": true
								}
							]
						}, {
							"logicalOr": true,
							"filterOps": [{
									"op": "=",
									"val": 80,
									"path": "src.port",
									"logicalOr": true
								}, {
									"op": "=",
									"val": 443,
									"path": "src.port",
									"logicalOr": true
								}
							]
						}
					]
				}
			],
			[{
					"desc": "> 5MB",
					"filterOps": [{
							"op": ">",
							"val": 5000000,
							"path": "totalbytes"
						}
					]
				}
			]
		];
		var $httpBackend, mycommonFilters, mySocketFlow;
		var mockFilterOp = {
			"foo": "bar"
		};
		beforeEach(function() {
			module(function($provide) {
				mySocketFlow = jasmine.createSpyObj('socketFlow', ['filterOp', 'addFilter', '$get', '$getEnts']);
				mySocketFlow.filterOp.andReturn(mockFilterOp);
				$provide.value('socketFlow', mySocketFlow);
			});
			inject(function($injector) {
				$httpBackend = $injector.get('$httpBackend');

				$httpBackend.when('GET', 'm/flowView/sampleFlowFilters.json').respond(sampleFilters);
			});
		});

		afterEach(function() {
			$httpBackend.verifyNoOutstandingExpectation();
			$httpBackend.verifyNoOutstandingRequest();
		});
		it('should retrieve sampleFlowFilters.json', function() {
			$httpBackend.expectGET('m/flowView/sampleFlowFilters.json');
			inject(function($injector) {
				mycommonFilters = $injector.get('commonFilters');
			});
			$httpBackend.flush();
		});
		describe('API', function() {
			beforeEach(function() {
				inject(function($injector) {
					mycommonFilters = $injector.get('commonFilters');
				});
				$httpBackend.flush();
			});
			it('should provide the json', function() {
				expect(mycommonFilters.getFilters()).toEqual(sampleFilters);
			});
			it('should have equal number of categories as sampleFilter arrays', function() {
				expect(mycommonFilters.getFilters().length).toEqual(mycommonFilters.getCategories().length);
			});
			it('should apply a simple sampleFilter', function() {
				var filters = mycommonFilters.getFilters();
				mycommonFilters.apply(filters[2][0]);
				expect(mySocketFlow.filterOp.mostRecentCall.args).toEqual([">", 5000000, "totalbytes", undefined]);
				expect(mySocketFlow.addFilter).toHaveBeenCalledWith(mockFilterOp);
			});

			it('should apply a nested sampleFilter', function() {
				var filters = mycommonFilters.getFilters();
				mycommonFilters.apply(filters[1][0]);

				expect(mySocketFlow.filterOp.callCount).toBe(4);
				expect(mySocketFlow.addFilter.calls[0].args[0]).toEqual({
					"logicalOr": true,
					"filterOps": [mockFilterOp, mockFilterOp]
				});
				expect(mySocketFlow.addFilter.calls[1].args[0]).toEqual({
					"logicalOr": true,
					"filterOps": [mockFilterOp, mockFilterOp]
				});
			});
			it('should apply a modifiable sampleFilter', function() {
				var filters = mycommonFilters.getFilters();
				mycommonFilters.apply(filters[0][0]);
				expect(mySocketFlow.filterOp.mostRecentCall.args).toEqual(["=", "10.0.0.1", "src.ip", undefined]);
				expect(mySocketFlow.addFilter).toHaveBeenCalledWith(mockFilterOp);
			});
		});
	});
});