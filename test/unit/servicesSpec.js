'use strict';

describe('Core services', function() {
  beforeEach(module('visdomNG'));

  describe('version', function() {
    it('should return current version', inject(function(version) {
      expect(version).toBeDefined();
    }));
  });

  describe('navService', function() {
    var mynavService;
    beforeEach(inject(function(navService) {
      mynavService = navService;
    }));
    //NOTE: Initialization is tough to test because of the run block on loading this module
    it('should provide an array of navs', function() {
      expect(mynavService.$get()).toBeDefined();
    });
    it('should register a nav', function() {
      var navs = mynavService.$get();
      mynavService.register({
        "foo": "bar"
      });
      expect(navs[navs.length - 1]).toEqual({
        "foo": "bar"
      });
    });
    it('should unregister a nav', function() {
      var navs = mynavService.$get();
      var nav = {
        "foo": "bar"
      };
      var badnav = {
        "bar": "foo"
      };
      mynavService.register(nav);
      expect(navs.length).toEqual(4);
      mynavService.unregister(nav);
      expect(navs.length).toEqual(3);
    });
    it('should handle unregistering non-existend navs', function() {
      var navs = mynavService.$get();
      var nav = {
        "foo": "bar"
      };
      var badnav = {
        "bar": "foo"
      };
      mynavService.register(nav);
      expect(navs.length).toEqual(4);
      mynavService.unregister(badnav);
      expect(navs.length).toEqual(4);
    });
  });

  describe('notify service', function() {
    var mynotify, navs, note, data;
    beforeEach(inject(function($templateCache, notify) {
      mynotify = notify;
      navs = mynotify.$get();
      data = {};
      note = {
        data: data,
        type: "test"
      };

    }));
    it('should provide an array of notifications', function() {
      expect(mynotify.$get()).toEqual([]);
    });
    it('should add a note', function() {
      var navs = mynotify.$get();
      mynotify.add(note);
      expect(navs[0]).toBe(note);
    });
    it('should handle notes sharing data or same note', function() {
      mynotify.add(note);
      expect(navs.length).toEqual(1);
      var newnote = {
        data: data,
        type: "foo"
      };
      mynotify.add(note);
      expect(navs.length).toEqual(1);
      expect(note.occurances).toEqual(2);
      mynotify.add(newnote);
      expect(navs.length).toEqual(2);
    });
    it('should acknowledge a note', function() {
      mynotify.add(note);
      expect(navs.length).toEqual(1);
      mynotify.acknowledge(note);
      expect(navs.length).toEqual(0);
    });
    it('should acknowledgeAll notes', function() {
      mynotify.add(note);
      var newnote = {
        data: "lolol",
        type: "bar"
      };
      mynotify.add(newnote);
      mynotify.acknowledgeAll();
      expect(navs.length).toEqual(0);
    })
    it('should handle unregistering non-existend navs', function() {
      var badnav = {};
      mynotify.add(note);
      expect(navs.length).toEqual(1);
      mynotify.acknowledge(badnav);
      expect(navs.length).toEqual(1);
    });
    it('should call ack callback on acknowledging a note', function() {
      var callback = jasmine.createSpy("ackCallback");
      var note = {
        ack: callback
      };
      mynotify.add(note);
      mynotify.acknowledge(note);
      expect(callback).toHaveBeenCalled();
    });
  });

  describe('whitelist', function() {
    var socketFlow, mywhitelist, notify, scope;
    var badflow = {
      1234: {
        list: "black",
        src: {
          ent: {
            list: "black"
          }
        },
        dst: {
          ent: {
            list: "black"
          }
        }
      }
    };
    beforeEach(module(function($provide) {
      socketFlow = jasmine.createSpyObj('socketFlow', ['$get', '$getEnts']);
      socketFlow.$get.andReturn(badflow);
      socketFlow.$getEnts.andReturn({
        1234: "fooEnt"
      });
      notify = jasmine.createSpyObj('notify', ['add']);
      $provide.value('socketFlow', socketFlow);
      $provide.value('notify', notify);
    }));
    beforeEach(inject(function(whitelist, $rootScope) {
      mywhitelist = whitelist;
      scope = $rootScope;
    }));
    describe('fingerprint', function() {
      var id;
      beforeEach(function() {
        mywhitelist.fingerprint();
        id = "1234";
      });
      it('should create a flowCreate listener on fingerprint', function() {
        scope.$broadcast("flowCreate", id);
        expect(notify.add.mostRecentCall.args[0].type).toEqual("New Flow");
      });
      it('should create a entityCreate listener on fingerprint', function() {
        scope.$broadcast("entityCreate", id);
        expect(notify.add.mostRecentCall.args[0].type).toEqual("New Host");
      });
      it('should create a flowUpdate blacklist listener on fingerprint', function() {
        scope.$broadcast("flowUpdate", id);
        expect(notify.add.calls.length).toEqual(3);
        expect(notify.add.calls[0].args[0].type).toEqual("Blacklisted Flow");
        expect(notify.add.calls[1].args[0].type).toEqual("Blacklisted Host");
        expect(notify.add.calls[2].args[0].type).toEqual("Blacklisted Host");
      });
    });
  });

  describe('Flow', function() {
    var $httpBackend, myflow;
    beforeEach(inject(function(_$httpBackend_, Flow) {
      $httpBackend = _$httpBackend_;
      myflow = Flow;
    }));
    it('should retrieve all flow information from the API', function() {
      $httpBackend.expectGET('/api/json').
      respond([{
          "flowID": 13,
          "info": {
            "src": "24.4.40.165:51624",
            "dst": "184.86.114.238:443",
            "sendbytes": 69783,
            "recvbytes": "0"
          }
        }, {
          "flowID": 2,
          "info": {
            "src": "24.4.40.165:36278",
            "dst": "74.125.224.85:443",
            "sendbytes": 110804,
            "recvbytes": "0"
          }
        }
      ]);
      var flows = myflow.query();
      $httpBackend.flush();
      flows.then(function(data) {
        expect(data.length).toBe(2);
      });
    });
    it('should retrieve specific flow information from the API', function() {
      $httpBackend.expectGET('/api/json?FlowID=13').
      respond({
        "flowID": 13,
        "info": {
          "src": "24.4.40.165:51624",
          "dst": "184.86.114.238:443",
          "sendbytes": 69783,
          "recvbytes": "0"
        }
      });

      var flows = myflow.query({
        FlowID: "13"
      });
      $httpBackend.flush();
      flows.then(function(dataz) {
        expect(dataz).not.toBe({});
      });
    });
  });


});