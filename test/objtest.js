"use strict";
//closure to create private variables
var foo = (function() {
	var priv = 5;
	return {
		getpriv: function() {
			return priv;
		},
		setpriv: function(x) {
			console.log("foo setting priv to " + x);
			priv = x;
		}
	};
}());

//if inheriting, the other objects will share those values
console.log("foo: " + foo.getpriv());
var bar = Object.create(foo);
console.log("bar: " + bar.getpriv());

foo.setpriv(8);
console.log("foo: " + foo.getpriv());
console.log("bar: " + bar.getpriv());


//manual properties. these default to false
/*properties can have attributes:
value: any value
writeable: bool
enumerable: bool
configurable: bool
get: func () return value
set: func (value)

top 4 apply to data properties
bottom 4 apply to accessor properties

*/

var my_object = Object.create(Object.prototype);

Object.defineProperty(my_object, 'foo', {
	value: "bar",
	writeable: true,
	enumerable: true,
	configurable: true
});



//accessor properties
console.log("Let's define a getter/setter");
my_object.mm = 987;

Object.defineProperty(my_object, 'inch', {
	get: function() {
		return this.mm / 25.4;
	},
	set: function(value) {
		this.mm = value * 25.4;
	},
	enumerable: true
});

console.log("It's enumerable");
console.log("keys = " + Object.keys(my_object));

console.log("but i can't change it");

try {
	Object.defineProperty(my_object, 'inch', {
		get: function() {
			return this.mm / 10; //lol i'm a marketing guy who fudges numbers
		},
		set: function(value) {
			this.mm = value * 10;
		},
		enumerable: true
	});
} catch (err) {
	console.log("error: " + err.message);
}

//Get property attributes
console.log("attributes of inch: " + Object.getOwnPropertyDescriptor(my_object, "inch"));



//getting all keys (including non enumerable)
var my_object = {
	mm: 99
};
Object.defineProperty(my_object, 'inch', {
	get: function() {
		return this.mm / 25.4;
	},
	set: function(value) {
		this.mm = value * 25.4;
	},
	enumerable: false
});

console.log("non enumerable inch:");
Object.keys(my_object).forEach(function(element, index, array) {
	console.log("\tkey " + index + ":" + element);
});
console.log("getOwnPropertyNames: " + Object.getOwnPropertyNames(my_object));

//object extensibility
console.log("----------- object extensibility ----------");
console.log("Object.getOwnPropertyNames(Object): " + Object.getOwnPropertyNames(Object));

// WTF THIS SHOULD WORK BUT ERRORS?!?!?!
//Object.preventExtentions(my_object); //disallow adding more properties. cannot be cleared once set
Object.seal(my_object); //same as preventextentions. turns off configurable bits for keys.
console.log("my_object is extensible: " + Object.isExtensible(my_object));
console.log("my_object is sealed: " + Object.isSealed(my_object));
console.log("my_object is frozen: " + Object.isFrozen(my_object));
try {
	my_object.newprop = "foobar";
} catch (err) {
	console.log("error: " + err.message);
}
try {
	my_object.mm = 90;
	console.log("I can still set mm to a new val: " + my_object.mm + "...but:");
	my_object.mm = function() {
		this.wontwork = true
	};
} catch (err) {
	console.log("error: " + err.message);
}

console.log("but now let's freeze");
Object.freeze(my_object); //same as seal but also turns off writeable bits
console.log("my_object is sealed: " + Object.isSealed(my_object));
console.log("my_object is frozen: " + Object.isFrozen(my_object));

try {
	my_object.mm = 90;
} catch (err) {
	console.log("error: " + err.message);
}


