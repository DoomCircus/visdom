'use strict';

/* http://docs.angularjs.org/guide/dev_guide.e2e-testing */

function appWindow() {
  return document.getElementsByTagName('iframe')[0].contentWindow;
}
function appAngular() {
  return appWindow().angular;
}

describe('visdom', function() {

  beforeEach(function() {
    browser().navigateTo('/app/index.html');
  });


  it('should automatically redirect to /version when location hash/fragment is empty', function() {
    expect(browser().location().url()).toBe("/version");
  });


  describe('view Version', function() {

    beforeEach(function() {
      browser().navigateTo('/app/#/version');
    });


    it('should render version when user navigates to /version', function() {
      expect(element('[ng-view] p:first').text()).
      toMatch(/This is the program info view/);
    });

  });

  describe('view flow detail', function() {
    beforeEach(function() {
      browser().navigateTo('/app/#/flows/1');
    });

    it('should display detailed flow information', function() {
      expect(element('[ng-view] p:first').text()).
      toMatch(/This is the detailed info view/);
    });

  });


  describe('view All flows', function() {
    var appwindow;

    beforeEach(function() {
      browser().navigateTo('/app/#/flows');
    });


    it('should render flows when user navigates to /flows', function() {
      expect(element('[ng-view] p:first', "First p in ng-view").text()).
      toMatch(/This is the flow view/);
    });
    it('should have more than zero flows', function() {
      expect(repeater('[ng-view] li').count()).toBeGreaterThan(0);
    });

    it('should have have less flows when filtered', function() {
      expect(repeater('[ng-view] li').count()).toBeGreaterThan(0);
      input('queryFlows').enter('199');
      expect(repeater('[ng-view] li').count()).toBeLessThan(4);
    });

    it('should navigate to flow detail if a flow is clicked on', function() {
      element('[ng-view] li:nth-child(1) p').click();
      expect(browser().location().url()).toMatch(/flows\/[0-9]/);
    });
  });
});