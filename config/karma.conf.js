basePath = '../';

files = [
  'app/lib/angular/angular.js',
  'app/lib/angular/angular-*.js',
  'app/lib/three.js',
  'app/lib/*.js',
  'app/js/*.js',
  'app/js/*/*.js',
  'app/m/**/module.js',
  'app/m/**/*.js',
  JASMINE,
  JASMINE_ADAPTER,
  'test/lib/angular/angular-mocks.js',
  'test/mocks/*.js',
  'test/unit/**/*.js'
  //'test/unit/m/flowView/controllersSpec.js'
];

autoWatch = false;

browsers = ['Chrome'];

junitReporter = {
  outputFile: 'test_out/unit.xml',
  suite: 'unit'
};
