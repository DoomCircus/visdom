/*====================================================================
Copyright 2013 Southfork Security, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
====================================================================*/
/* Services */

visdomNG.value('version', '0.2.0');

/* Services */

visdomNG.factory('Flow', ['$http', '$log',
  function($http, $log) {
    'use strict';
    var Flow = function(data) {
      angular.extend(this, data);
    };
    Flow.query = function(parms) {
      return $http.get('/api/json', {
        params: parms
      });
    };
    return Flow;
  }
]);

visdomNG.factory('navService', function() {
  //expect navs of form: { "text": < text for link>, opts: <dialog options>, click: <function to execute>}
  var navItems = [];
  return {
    register: function(item) {
      navItems.push(item);
    },
    unregister: function(item) {
      if (navItems.indexOf(item) != -1) {
        navItems.splice(navItems.indexOf(item), 1);
      }
    },
    $get: function() {
      return navItems;
    }
  }
});

visdomNG.factory('notify', [
  function() {
    'use strict';
    //expected notification is of the form: 
    /*{
    "type": < text > ,
    "message": < text > ,
    "data": < data object > ,
    "templateURL": < passed to ng - include with note in scope > ,
    "ack": < callback function on ack >
  }*/
    var notes = [];
    var notify = {};

    notify.$get = function() {
      return notes;
    };
    notify.add = function(note) {
      var existing = false;
      notes.forEach(function(val, ind, arr) {
        if (val.data === note.data) {
          if (val.type === note.type) {
            existing = true;
            val.occurances += 1;
          }
        }
      });
      if (!existing) {
        note.occurances = 1;
        notes.push(note);
      }
    };
    notify.acknowledge = function(note) {
      if (notes.indexOf(note) != -1) {
        if (note.ack !== undefined) {
          note.ack();
        }
        notes.splice(notes.indexOf(note), 1);
      }
    };
    notify.acknowledgeAll = function() {
      notes.forEach(function(val, ind, arr) {
        if (val.ack !== undefined) {
          val.ack();
        }
      });
      notes.length = 0;
    }
    return notify;
  }
]);

visdomNG.factory('whitelist', ['socketFlow', 'notify', '$rootScope',
  function(socketFlow, notify, $rootScope) {
    var flowurl = 'm/flowView/partials/flowNotification.html';
    var enturl = 'm/entView/partials/entNotification.html';
    var fingerprinted = false;
    var flows = socketFlow.$get();
    var ents = socketFlow.$getEnts();
    var note;

    var whitelist = {};
    whitelist.fingerprint = function() {
      // console.log("fingerprinted");
      fingerprinted = true;
      $rootScope.$on('flowCreate', function(e, flowID) {
        note = {
          type: "New Flow",
          data: flows[flowID],
          templateURL: flowurl
        };
        notify.add(note);
      });
      $rootScope.$on('entityCreate', function(e, entID) {
        note = {
          type: "New Host",
          data: ents[entID],
          templateURL: enturl
        };
        notify.add(note);
      });
    };
    $rootScope.$on('flowUpdate', function(e, flowID) {
      var flow = flows[flowID];
      if (flow.list === "black") {
        note = {
          type: "Blacklisted Flow",
          data: flow,
          templateURL: flowurl
        };
        notify.add(note);
      }
      if (flow.src.ent.list === "black") {
        note = {
          type: "Blacklisted Host",
          data: flow.src.ent,
          templateURL: enturl
        };
        notify.add(note);
      }
      if (flow.dst.ent.list === "black") {
        note = {
          type: "Blacklisted Host",
          data: flow.dst.ent,
          templateURL: enturl
        };
        notify.add(note);
      }
    });
    return whitelist;
  }
]);