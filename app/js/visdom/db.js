/*====================================================================
Copyright 2013 Southfork Security, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
====================================================================*/
//--------------------
// group and entity functions
//--------------------
/** TODO: NOTE: CWT: if we are going to use particle systems for entities, we will need
 * to incorporate custom attributes to a vertex shader so that the computation for sparks
 * and changing vertices is done on the GPU
 *
 * the alternative is to use a mesh for all of the entities, as well as groups. This might
 * work if we hide the entities when zoomed out of a group, but that requires everything to
 * have a group. (right now we do that anyway so....maybe it's fine)
 **/
//TODO: create a parent class visdomObj with stuff like isSelected?? they each implement 
// * selection/mouseover/etc differently though, so probably not? But maybe they should all
// * be the same thing. A group is just a group of groups? Since we have the phase in/out
// * when the camera is close....seems like that is a better, more modular solution. Need
// * to refactor


VISDOM.group = function(id) {
  this.id = id;
  this.name = "grp " + this.id;
  this.mesh = null;
  this.particleSystem = null;
  this.vertexIndex = 0; //tracks which vertices of the particleSystem are in use. hand one out when an entity is added
  this.isSelected = false;
  this.freshness = VISDOM.params.OBJLIFESPAN;
};

VISDOM.group.prototype = {
  constructor: VISDOM.group,
  setName: function(name) {
    this.name = name;
    return this;
  },
  setPostition: function(vector) {
    this.mesh.position.copy(vector);
    return this;
  },
  calcPosition: function() {
    //Calculate the position in 3D for this vertex
    this.mesh.position.x = (Math.random() * 200 - 100);
    this.mesh.position.y = (Math.random() * 200 - 100);
    this.mesh.position.z = (Math.random() * 200 - 100);

    this.mesh.updateMatrixWorld();
  },
  makeCube: function(size, numverts, myscene) {
    myscene = myscene || VISDOM.scene;
    size = size || VISDOM.params.GROUPSIZE;
    numverts = numverts || VISDOM.params.GROUPENTCOUNT;
    var geometry = new THREE.CubeGeometry(size, size, size, 4, 4, 4);
    //var geometry = new THREE.SphereGeometry(10, 16, 16);
    var tmpcolor = VISDOM.params.colors.cube;
    var material = new THREE.MeshPhongMaterial({
      color: tmpcolor,
      opacity: 0.8
    });
    material.transparent = true;
    this.mesh = new THREE.Mesh(geometry, material);
    this.mesh.visdomObj = this; //keep back reference to the group

    this.calcPosition();

    this.createParticlesystem(numverts);

    this.mesh.add(this.particleSystem);
    myscene.add(this.mesh);

    this.calcEntPosition = function(ent) {
      ent.vertex.x = (Math.random() * 10 - 5);
      ent.vertex.y = (Math.random() * 10 - 5);
      ent.vertex.z = (Math.random() * 10 - 5);

      this.particleSystem.geometry.verticesNeedUpdate = true;
    };

    return this;
  },
  makeGeo: function(size, numverts, myscene) {
    myscene = myscene || VISDOM.scene;
    size = size || VISDOM.params.GROUPSIZE;
    numverts = numverts || VISDOM.params.GROUPENTCOUNT;
    //var geometry = new THREE.CubeGeometry(size, size, size, 4, 4, 4);
    var geometry = new THREE.PlaneGeometry(VISDOM.params.MAPWIDTH, VISDOM.params.MAPHEIGHT, 10, 6);
    var tmpcolor = VISDOM.params.colors.cube;
    // var texture = THREE.ImageUtils.loadTexture('textures/Mercator_projection-1207x1024.jpg');
    var texture = THREE.ImageUtils.loadTexture('textures/mercator.png');
    texture.anisotropy = VISDOM.renderer.getMaxAnisotropy();

    var material = new THREE.MeshBasicMaterial({
      map: texture
    });
    material.opacity = 0.7;
    material.transparent = true;
    this.mesh = new THREE.Mesh(geometry, material);
    this.mesh.visdomObj = this; //keep back reference to the group

    this.mesh.position.x = 0;
    this.mesh.position.y = -100;
    this.mesh.position.z = -300;

    this.calcEntPosition = function(ent) {};

    //Geo location to x,y coords
    this.geolocateEnt = function(ent, location) {
      var longitude_shift = 0; // number of pixels your map's prime meridian is off-center.
      var x_pos = VISDOM.params.MAPWIDTH / 2;
      var y_pos = VISDOM.params.MAPHEIGHT / 2;
      var map_width = VISDOM.params.MAPWIDTH;
      var map_height = VISDOM.params.MAPHEIGHT;
      var lat = parseFloat(location.lat);
      var lng = parseFloat(location.long);
      var x, y;

      // longitude: just scale and shift
      x = (map_width * (180 + lng) / 360) % map_width + longitude_shift;

      // latitude: using the Mercator projection
      lat = lat * Math.PI / 180; // convert from degrees to radians
      y = Math.log(Math.tan((lat / 2) + (Math.PI / 4))); // do the Mercator projection (w/ equator of 2pi units)
      y = (map_height / 2) - (map_width * y / (2 * Math.PI)); // fit it to our map

      x -= x_pos;
      y -= y_pos;
      y = y * -1;

      var vect = new THREE.Vector3();
      vect.x = x;
      vect.y = y;
      vect.z = 1; //slightly in front of the map

      ent.vertex.copy(vect);

      this.particleSystem.geometry.verticesNeedUpdate = true;
    };

    this.createParticlesystem(numverts);

    this.mesh.add(this.particleSystem);
    myscene.add(this.mesh);
    this.mesh.updateMatrixWorld();

    return this;
  },
  createParticlesystem: function(numVerts) {
    var geometry = new THREE.Geometry();
    for (var i = 0; i < numVerts; i++) {
      var vertex = new THREE.Vector3(9999999, 9999999, 9999999); //hide from camera
      geometry.vertices.push(vertex);
    }
    var material = new THREE.ParticleBasicMaterial({
      size: 1,
      color: VISDOM.params.colors.cube_highlight
    });

    this.particleSystem = new THREE.ParticleSystem(geometry, material);
  },
  addEntity: function(entity) {
    var newvert, tmpent, oldsystem, oldlen;
    var x;
    if (entity === undefined) { //wut?
      console.log("group.addEntity tried to add undefined. you may have a problem");
      return undefined;
    }
    if (this.vertexIndex >= this.particleSystem.geometry.vertices.length - 1) {

      oldsystem = this.particleSystem;
      oldlen = oldsystem.geometry.vertices.length;
      this.createParticlesystem(oldlen * 2);
      //clean up deleted entities while we're at it. TODO: make vertices re-usable?
      this.vertexIndex = 0;
      for (x = 0; x < oldlen - 1; x++) {
        if (oldsystem.geometry.vertices[x].visdomObj !== undefined) {
          newvert = this.particleSystem.geometry.vertices[this.vertexIndex];
          newvert.copy(oldsystem.geometry.vertices[x]);
          tmpent = oldsystem.geometry.vertices[x].visdomObj;
          tmpent.vertex = newvert;
          newvert.visdomObj = tmpent;
          this.vertexIndex++;
        }
      }
      //free up the memory.
      this.mesh.remove(oldsystem);
      this.mesh.add(this.particleSystem);

      oldsystem.geometry.dispose();
      oldsystem.material.dispose();
      console.log("Too many entities for group. allocating new geometry");
    }

    newvert = this.particleSystem.geometry.vertices[this.vertexIndex];
    //TODO: put the new entity vertex in an intelligent position on the group
    // perhaps add functions to arrange the entities
    newvert.visdomObj = entity;
    entity.vertex = newvert;
    this.vertexIndex++;

    if (this.hasOwnProperty("calcEntPosition")) {
      this.calcEntPosition(entity);
    } else {
      // entity.calcPosition();
      console.log("addEntity: no entity position calculator set");
    }

    return newvert;
  },
  removeEntity: function(entity) {
    //TODO: actually remove from the group. This would require giving back vertices and it's easier
    //  to just let them sit there and expire when the group is removed
    entity.vertex.x = 9999999;
    entity.vertex.y = 9999999;
    entity.vertex.z = 9999999;
    delete entity.vertex.visdomObj; //remove reference
    this.particleSystem.geometry.verticesNeedUpdate = true;
  },
  touch: function() {
    if (this.freshness < VISDOM.params.OBJLIFESPAN) {
      this.freshness = VISDOM.params.OBJLIFESPAN;
    }
    //TODO:change this to an alpha calc function that takes into account things like group phase distance
    this.mesh.material.opacity = 1.0;
  },
  toggleSelected: function() {
    // this.mesh.currentHex = this.mesh.currentHex ^ 0xBB0099;
    // this.mesh.material.emissive.setHex(this.mesh.currentHex);
    VISDOM.$scope.$emit('vis3dNav', "entities/" + this.id);
  },
  onMouseIn: function() {
    this.mesh.currentHex = this.mesh.material.emissive.getHex();
    this.mesh.material.emissive.setHex(0x770055);
    VISDOM.hud.addTooltip(this.name);
  },
  onMouseOut: function() {
    this.mesh.material.emissive.setHex(this.mesh.currentHex);
    VISDOM.hud.removeTooltip();
  },
  forEachEnt: function(callback) { //helper for each entity function
    var verts = this.particleSystem.geometry.vertices;
    for (var x = 0; x <= this.vertexIndex; x++) {
      if (verts[x].visdomObj !== undefined) {
        callback(verts[x].visdomObj);
      }
    }
  }
};

VISDOM.entity = function(id, group) {
  this.id = id;
  this.group = group;
  this.vertex = group.addEntity(this);
  this.name = "ent " + this.id;
  this.isSelected = false;
  this.freshness = VISDOM.params.OBJLIFESPAN;
};

VISDOM.entity.prototype = {
  constructor: VISDOM.entity,
  setGroup: function(group) {
    //TODO: this needs to be changed to moveGroup and lots of upkeep done to clean up moving a vertex between meshes
    this.group = group;
    group.addEntity(this);
    return this;
  },
  setVertex: function(vertex) {
    this.vertex.copy(vertex);
    return this;
  },
  getWorldV3: function() {
    //return this.group.mesh.matrixWorld.multiplyVector3(this.vertex.clone());
    var newvert = this.vertex.clone();
    return newvert.applyMatrix4(this.group.mesh.matrixWorld);
  },
  touch: function() {
    this.freshness = VISDOM.params.OBJLIFESPAN;
    this.group.touch();
  },
  calcPosition: function() {
    //Calculate the position in 3D for this vertex
    this.vertex.x = (Math.random() * 10 - 5);
    this.vertex.y = (Math.random() * 10 - 5);
    this.vertex.z = (Math.random() * 10 - 5);

    this.group.particleSystem.geometry.verticesNeedUpdate = true;
  },
  toggleSelected: function() {
    var visdomNGID = this.id.substring(0, this.id.indexOf(':'));
    VISDOM.$scope.$emit('vis3dNav', "entities/" + visdomNGID);
  },
  onMouseIn: function() {
    VISDOM.hud.addTooltip(this.name);
  },
  onMouseOut: function() {
    VISDOM.hud.removeTooltip();
  }
};

//-----------------
// HELPER FUNCTIONS
//-----------------
//get group or create if not yet seen
//TODO: refactor to pass in the group list
VISDOM.getGroupByID = function(id) {
  "use strict";
  if (id === undefined) {
    return undefined;
  }
  if (VISDOM.groups[id] === undefined) {
    VISDOM.groups[id] = new VISDOM.group(id);
    //make cube by default I guess?
    VISDOM.groups[id].makeCube();
    return VISDOM.groups[id];
  } else {
    VISDOM.groups[id].touch();
    return VISDOM.groups[id];
  }
};

//get entity or create if not yet seen
//entity ID is intended to be a unique key. Maybe the id+group should be the unique key?
//NOTE: does not move entity to the group given if it already exists
//TODO: refactor to pass in the entity list
VISDOM.getEntityByID = function(id, group) {
  "use strict";
  group = group || VISDOM.params.DEFAULTGROUP;
  if (id === undefined) {
    return undefined;
  }
  if (VISDOM.entities[id] === undefined) {
    VISDOM.entities[id] = new VISDOM.entity(id, VISDOM.getGroupByID(group));
    return VISDOM.entities[id];
  } else {
    VISDOM.entities[id].touch();
    VISDOM.entities[id].group.touch();
    return VISDOM.entities[id];
  }
};

VISDOM.removeGroup = function(group) {
  "use strict";
  //NOTE: removing an entity/group that has associated flames will break things. make sure we check for that
  // before aging out an entity/group. Although, a group with a flame should always be fresh so...

  if (group === undefined) { //trying to delete a group that doesn't exist
    return undefined;
  }

  VISDOM.scene.remove(group.particleSystem);
  VISDOM.scene.remove(group.mesh);

  group.mesh.geometry.dispose();
  group.mesh.material.dispose();
  group.particleSystem.geometry.dispose();
  group.particleSystem.material.dispose();
};

VISDOM.removeEntity = function(entity) {
  "use strict";
  //NOTE: removing an entity/group that has associated flames will break things. make sure we check for that
  // before aging out an entity/group
  if (entity === undefined) {
    return undefined;
  }
  entity.group.removeEntity(entity);
};

VISDOM.updateDBObjs = function(entlist, grouplist) {
  "use strict";
  var g; //group
  var e; //entity
  var value;
  var remgrp = function() {
    VISDOM.removeGroup(this.myG);
  };
  var updateAlpha = function() {
    this.myG.mesh.material.opacity = this.alpha;
  };

  for (value in grouplist) {
    var tweenOut;
    g = grouplist[value];
    g.mesh.rotation.x += VISDOM.params.group.spinrate;
    g.mesh.rotation.y += VISDOM.params.group.spinrate;

    g.freshness--;

    if (g.freshness === 0) {
      // console.log("Removing group " + value);
      delete grouplist[value];

      tweenOut = new TWEEN.Tween({
        alpha: 1,
        myG: g,
        myVal: value
      })
        .to({
        alpha: 0.01
      }, 1000)
        .onUpdate(updateAlpha)
        .onComplete(remgrp).start();
    }
    // g.forEachEnt(function(ent){ 
    //   console.log("I'm a foreach: " + ent.id);
    // });
  }
  for (value in entlist) {
    entlist[value].freshness--;
    if (entlist[value].freshness === 0) {
      VISDOM.removeEntity(entlist[value]);
      delete entlist[value];
    }
  }

  return true;
};