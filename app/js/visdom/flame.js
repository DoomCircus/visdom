/*====================================================================
Copyright 2013 Southfork Security, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
====================================================================*/
/*jslint browser: true, devel: true, plusplus: true, white: true, passfail: false*/

VISDOM.flame = function(type) {
  this.type = type;
  this.threeObjs = []; //array for storing threejs objects to deallocate when done
  //TODO: come up with method/object structure to tie flames to arbitrary numbers of entities or groups
  this.visdomObjs = []; //array for tracking targets
  this.id = VISDOM.flames.push(this)-1;
};

VISDOM.flame.prototype = {
  constructor: VISDOM.flame,
  makeLine: function(e1, e2) {
    this.visdomObjs.push(e1);
    this.visdomObjs.push(e2);
    
    var tmpcolor = 0xffffff * Math.random();
    var lineMat = new THREE.LineBasicMaterial({
      color: tmpcolor,
      opacity: 0.7,
      linewidth: 1
    });

    var v1 = e1.getWorldV3();
    var v2 = e2.getWorldV3();

    var geom = new THREE.Geometry();
    geom.vertices.push(v1);
    geom.vertices.push(v2);

    var line = new THREE.Line(geom, lineMat);
    this.threeObjs.push(line);

    VISDOM.scene.add(line);
    return true;
  },
  makeCurve: function(e1, e2) {
    this.visdomObjs.push(e1);
    this.visdomObjs.push(e2);
    // smooth my curve over this many points
    var numPoints = 100;
    var v1 = e1.getWorldV3();
    var v2 = e2.getWorldV3();
    var vtmp = null;

    var points = [];
    points.push(v1);

    //TODO: make curve intelligently choose intermediate points
    vtmp = v1.clone();
    vtmp.y += 5;
    vtmp.x += 5;
    points.push(vtmp);
    vtmp = v2.clone();
    vtmp.y += 5;
    vtmp.x += 5;
    points.push(vtmp);

    points.push(v2);

    spline = new THREE.SplineCurve3(points);

    var tmpcolor = 0xffffff * Math.random();
    var material = new THREE.LineBasicMaterial({
      color: tmpcolor,
      opacity: 0.8,
      linewidth: 2
    });
    var geometry = new THREE.Geometry();
    var splinePoints = spline.getPoints(numPoints);

    for(var i = 0; i < splinePoints.length; i++) {
      geometry.vertices.push(splinePoints[i]);
    }

    var line = new THREE.Line(geometry, material);
    this.threeObjs.push(line);
    VISDOM.scene.add(line);

    //these functions provide a vertex at a percentage of the curve.
    //console.log(spline.getPoint(0.5));
    //console.log(spline.getTangent(0.5));

    return true;
  },
  update: function() {
    //TODO: every frame we have to reconcile values for this line with the new positions of the entities
    // see above todo about tying flame properties to threeobjs (positionally that is)
    //console.log("Updating " + this.visdomObjs[0].id + " + " + this.visdomObjs[1].id);
    this.visdomObjs.forEach(function(value, index, array){
      value.touch();
    });
  },
  extinguish: function() {
    this.threeObjs.forEach(function(value, index, array) {
      VISDOM.scene.remove(value);
      //free up the memory.
      value.geometry.dispose();
      value.material.dispose();
    });
    this.threeObj = [];
    this.visdomObjs = [];

    //NOTE: shrink array or change to an object? array.forEach should skip a delted node fine
    delete VISDOM.flames[this.id];

  }

};

//-----------------
// Helper functions
//-----------------
VISDOM.createLineFlame = function(e1, e2) {
  var flame = new VISDOM.flame("line");
  flame.makeLine(e1, e2);
  console.log("VISDOM.createLineFlame");
  return flame;
};

VISDOM.createCurveFlame = function(e1, e2) {
  var flame = new VISDOM.flame("curve");
  flame.makeCurve(e1, e2);
  console.log("VISDOM.createCurveFlame");
  return flame;
};

VISDOM.updateFlames = function() {
  //TODO: fix vertices, curve endpoints, etc. requires an update function for each flame type or some
  // method of attaching a flame vertex to an object vertex (and any inbetween points)
  VISDOM.flames.forEach(function(value, index, array) {
    value.update();
  });
};