/*====================================================================
Copyright 2013 Southfork Security, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
====================================================================*/
/*jslint browser: true, devel: true, plusplus: true, white: true, passfail: false*/
/** @param {VISDOM.entity} entity1 First Vector3 to draw spark between.
 *  @param {VISDOM.entity} entity2 Second Vector3 to draw spark between.
 *  @return {bool}
 **/
VISDOM.createLineSpark = function(e1, e2) {
  "use strict";
  //NOTE: might need to use matrixworld positions instead?
  //NOTE: Potential optimization exists here. Right now we are creating a new geometry for every line
  // that we are going to draw. If instead we set up a large geometry and just used vertices and moved
  // them around to draw lines, we wouldn't have to deal with deallocating memory and creating new
  // geometries. This would, however, require coding overhead to get a useable set of vertices to draw
  // a line between and handle moving them out of vision, etc.

  var tmpcolor = 0xffffff * Math.random();
  var lineMat = new THREE.LineBasicMaterial({
    color: tmpcolor,
    transparent: true,
    opacity: 0.1,
    linewidth: 2
  });

  var v1 = e1.getWorldV3();
  var v2 = e2.getWorldV3();

  var geom = new THREE.Geometry();
  geom.vertices.push(v1);
  geom.vertices.push(v2);

  var line = new THREE.Line(geom, lineMat);

  //setup fadein tween
  var tweenIn = new TWEEN.Tween({
    alpha: 0.1
  }).to({
    alpha: 1.0
  }, 250).onUpdate(function() {
    lineMat.opacity = this.alpha;
    //console.log("tween update?" + this.alpha);
  }).start();

  var tweenOut = new TWEEN.Tween({
    alpha: 1
  }).to({
    alpha: 0.1
  }, 250).onUpdate(function() {
    lineMat.opacity = this.alpha;
    //console.log("tween update?" + this.alpha);
  }).onComplete(function() {
    //console.log("removeing line");
    VISDOM.scene.remove(line);

    //free up the memory.
    line.geometry.dispose();
  });
  tweenIn.chain(tweenOut);

  VISDOM.scene.add(line);
  return true;
};