/*====================================================================
Copyright 2013 Southfork Security, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
====================================================================*/
//-----------------
// HUD functions
//-----------------
VISDOM.HUD = function() {
  this.datGUI = this.initDatGui();
  //TODO: move tooltip to angular
  $(".tooltip").hide();
};

VISDOM.HUD.prototype.update = function() {
  //updateTooltip();
};

VISDOM.HUD.prototype.addTooltip = function(tip) {
  if(tip === undefined) {
    tip = "Oh Hai!";
  }
  //TODO: move tooltip to angular
  $(".vistooltip").text(tip).fadeIn(100);
};

VISDOM.HUD.prototype.removeTooltip = function() {
  //TODO: move tooltip to angular
  $(".vistooltip").fadeOut(100);
};

VISDOM.HUD.prototype.updateTooltip = function() {
  //TODO: use angular to set the tooltip text
  if(VISDOM.renderer === undefined || $(".tooltip").css("display") === "none") {
    return;
  }
  var pos = this.mouseToDomPos(VISDOM.renderer.domElement);
  //TODO: move tooltip to angular
  $(".vistooltip").css({
    'left': pos.x + 'px',
    'top': pos.y - 40 + 'px'
  });
};

VISDOM.HUD.prototype.initDatGui = function() {
  //parameters to control with dat.gui
  var gui = new dat.GUI();
  var f1 = gui.addFolder('Groups');
  f1.add(VISDOM.params.group, 'phaseDistance');
  f1.add(VISDOM.params.group, 'spinrate');

  var f2 = gui.addFolder('Testview');
  f2.add(VISDOM.params.db, 'sparkRate', 0, 25);
  f2.add(VISDOM.params.db, 'newEntityRate', 0.00, 0.9);
  f2.add(VISDOM.params.db, 'newGroupRate', 0.000, 0.9);

  var f3 = gui.addFolder('Movement');
  f3.add(VISDOM.params.movement, 'controls', ['Trackball', 'Flycontrol', 'FirstPerson']).onChange(VISDOM.setControls.bind(VISDOM));
  f3.add(VISDOM.params.movement, 'flySpeed', 50, 250).onChange(function() {
    if(VISDOM.params.movement.controls === 'Flycontrol') {
      VISDOM.controls.movementSpeed = VISDOM.params.movement.flySpeed;
    }
  });
  f3.add(VISDOM.params.movement, 'flyRollSpeed', 0.2, 1.0).onChange(function() {
    if(VISDOM.params.movement.controls === 'Flycontrol') {
      VISDOM.controls.rollSpeed = VISDOM.params.movement.flyRollSpeed;
    }
  });

  gui.close();
  return gui;
};


//-----------------
// 3D to HUD Helpers
//-----------------
VISDOM.HUD.prototype.toScreenXY = function(position, camera, canvas) {
  var pos = position.clone();
  var projScreenMat = new THREE.Matrix4();
  camera.updateMatrixWorld();
  projScreenMat.multiply(camera.projectionMatrix, camera.matrixWorldInverse);
  pos.applyProjection(camera.projectionMatrix);

  return {
    x: (pos.x + 1) * canvas.width / 2 + canvas.offsetLeft,
    y: (-pos.y + 1) * canvas.height / 2 + canvas.offsetTop
  };
};

VISDOM.HUD.prototype.mouseToDomPos = function(canvas) {
  return {
    x: (VISDOM.mouse.x + 1) * canvas.width / 2 + canvas.offsetLeft,
    y: (-VISDOM.mouse.y + 1) * canvas.height / 2 + canvas.offsetTop
  };
};
