/*====================================================================
Copyright 2013 Southfork Security, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
====================================================================*/
var VISDOM = VISDOM || {
  revision: "0"
};

//---------------
//  DEBUG
//---------------
// Create a new object, that prototypally inherits from the Error constructor.
VISDOM.err = function(message) {
  this.name = "VISDOMerror";
  this.message = message || "VISDOM: unknown error";
};
VISDOM.err.prototype = new Error();
VISDOM.err.prototype.constructor = VISDOM.err;

//--------------------
//THREE init and helper functions
//--------------------

VISDOM.init = function(attachDom) {
  'use strict';
  //Stats Init
  this.stats = new Stats();
  var stats = this.stats;
  // 0: fps, 1: ms
  stats.setMode(0);

  // Align top-left
  stats.domElement.style.position = 'absolute';
  stats.domElement.style.left = (window.innerWidth - 80) + 'px';
  stats.domElement.style.top = '0px';

  attachDom.appendChild(stats.domElement);
  //end Stats init

  //VISDOM globals
  //NOTE: We've alternated between entities and groups as an object and as an arry
  // objects give you more flexibility in terms of what your "id" looks like. Array requires
  // an integer but are much faster. Running performance tests show no noticable difference,
  // however, so we're sticking with objects for now.
  this.entities = {};
  this.groups = {};
  this.flames = []; //flames should be part of the visdomOBJ (and let GC handle) rather than a separate array?
  this.sparkQueue = []; //shouldn't need sparkQueue. Sparks will remove themselves with tweens, setintervals, or callbacks?
  this.params = {
    group: {
      phaseDistance: 20000,
      spinrate: 0.0004
    },
    db: {
      sparkRate: 0,
      newGroupRate: 0.00,
      newEntityRate: 0.00
    },
    movement: {
      controls: 'Trackball',
      trackballZoomSpeed: 3,
      flySpeed: 100,
      flyRollSpeed: 0.8
    }
  };
  this.params.colors = {
    cube: 0x2B2BD6,
    cube_highlight: 0x8C22E3
  };
  this.params.GROUPENTCOUNT = 254;
  this.params.GROUPSIZE = 10;
  this.params.DEFAULTGROUP = 0;
  this.params.OBJLIFESPAN = 1000; //in frames
  this.params.MAPHEIGHT = 512;
  this.params.MAPWIDTH = 603;

  this.clock = new THREE.Clock();

  this.scene = new THREE.Scene();
  var scene = this.scene;
  //TODO: figure out what proper FOV should be
  this.camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 0.1, 10000);
  var camera = this.camera;

  this.renderer = new THREE.WebGLRenderer();
  var renderer = this.renderer;
  renderer.setSize(window.innerWidth, window.innerHeight);
  renderer.setClearColor(0x000000, 1);

  renderer.domElement.classList.add("visCanvas");
  attachDom.appendChild(renderer.domElement);

  camera.position.z = 60;

  //for ray tracing to detect object clicks
  this.projector = new THREE.Projector();
  this.INTERSECTED = undefined;
  this.SELECTED = undefined;
  this.mouse = {
    x: 0,
    y: 0
  };

  this.setControls();

  //Lighting
  scene.fog = new THREE.Fog(0x000000, 1, 15000);

  this.dLight = new THREE.DirectionalLight(0xffffff);
  this.dLight.position.set(camera.position.x, camera.position.y, camera.position.z).normalize();
  scene.add(this.dLight);

  var light = new THREE.AmbientLight(0x222222);
  scene.add(light);

  //event listeners
  window.addEventListener('resize', this.onWindowResize.bind(this), false);
  renderer.domElement.addEventListener('mousemove', this.onDocumentMouseMove.bind(this), false);
  renderer.domElement.addEventListener('mousedown', this.onDocumentMouseDown.bind(this), false);
  renderer.domElement.addEventListener('mouseup', this.onDocumentMouseUp.bind(this), false);

  //listen for camera change: NOTE: not fully supported yet, see delcaration todo
  document.addEventListener('camChange', this.phaseGroups.bind(this), false);

  //GeoIP Group Init
  this.groups["GEOLOCATED"] = new VISDOM.group("GEOLOCATED");
  var geo = this.groups["GEOLOCATED"];
  Object.defineProperty(this.groups, "GEOLOCATED", {
    enumerable: false
  }); //remove the geolocated group from iterative stuff
  geo.makeGeo();
  geo.mesh.children[0].visible = true;

  //Hud init
  this.hud = new VISDOM.HUD();

  return renderer.domElement;
}

// Helper functions

VISDOM.findIntersections = function() {
  /*TODO: threejs doesn't support intersection with particles/sprites yet
   * A fork has support for sprites: https://github.com/zevarito/three.js/commit/df0c872dda98aa51669ad87301b7eea8d85625c3
   * Right now we will hold off on detecting collision/selection for entities in 3D.
   * Will have to add code manually if not. Mr. Doob says Rays do not support intersection
   * with particlesystems "yet!". So functionality should be coming in the future. Will avoid for now and finish when it's done
   */
  // find intersections
  var camera = this.camera;
  var vector = new THREE.Vector3(this.mouse.x, this.mouse.y, 1);
  this.projector.unprojectVector(vector, camera);
  var ray = new THREE.Raycaster(camera.position, vector.sub(camera.position).normalize());
  var localRay = new THREE.Ray();

  var threshold = 1.5;
  var intersects = ray.intersectObjects(this.scene.children);

  if (intersects.length > 0) {
    //kind of a hack to know if we're looking through a cube and therefore expanded?
    if (intersects[0].object.visible === true && intersects[0].object.visdomObj.id !== "GEOLOCATED") {
      if (this.INTERSECTED != intersects[0].object) {
        if (this.INTERSECTED !== undefined && this.INTERSECTED.hasOwnProperty('visdomObj')) {
          this.INTERSECTED.visdomObj.onMouseOut();
        }
        this.INTERSECTED = intersects[0].object;

        this.INTERSECTED.visdomObj.onMouseIn();
      }
    } else { //particles!
      //TODO: clean this shit up
      localRay.copy(ray.ray);
      var obj = intersects[0].object.children[0];
      var vertices = obj.geometry.vertices;
      var found = undefined;
      var dot, intersect, distance;
      var v0 = new THREE.Vector3(),
        v1 = new THREE.Vector3(),
        v2 = new THREE.Vector3();
      for (var j = 0; j < vertices.length; j++) {
        var worldVect = vertices[j].clone();
        worldVect.applyMatrix4(obj.matrixWorld);
        //var worldVect = obj.matrixWorld.multiplyVector3(vertices[j].clone());

        v0.subVectors(worldVect, localRay.origin);
        dot = v0.dot(localRay.direction);

        intersect = v1.addVectors(localRay.origin, v2.copy(localRay.direction).multiplyScalar(dot));
        distance = worldVect.distanceTo(intersect);

        if (distance < threshold) { //found our particle
          if (this.INTERSECTED != vertices[j]) {
            if (this.INTERSECTED !== undefined && this.INTERSECTED.hasOwnProperty('visdomObj')) {
              this.INTERSECTED.visdomObj.onMouseOut();
            }
            this.INTERSECTED = vertices[j];
            this.INTERSECTED.visdomObj.onMouseIn();
          }
          found = this.INTERSECTED;
          break;
        }
      }
      if (found === undefined && this.INTERSECTED !== undefined) {
        if (this.INTERSECTED !== undefined && this.INTERSECTED.hasOwnProperty('visdomObj')) {
          this.INTERSECTED.visdomObj.onMouseOut();
        }
        this.INTERSECTED = undefined;
      }
    } //end nasty particle mess
  } else {
    if (this.INTERSECTED !== undefined) {
      this.INTERSECTED.visdomObj.onMouseOut();
    }
    this.INTERSECTED = undefined;
  }
}

VISDOM.phaseGroups = function() {
  /* TODO: phasing out the group will still cause it to be intersected by a ray. Need to add
   * support for skipping the first intersection if it's on an invisible group. this is seldom though so...who cares?
   * group: switch to worldMatrix? from mrDoob:
        var point1 = camera.matrixWorld.getPosition().clone();
        var point2 = cubeMesh.matrixWorld.getPosition().clone();
        var distance = point1.distanceTo( point2 );
   */
  var phaseDistance = this.params.group.phaseDistance;
  var halfPhaseDistance = phaseDistance / 2;
  var maxOpacityPhaseDistance = halfPhaseDistance * 1.2;
  var camera = this.camera;

  var campos = camera.position.clone();
  var objpos = campos.clone();

  this.dLight.position.copy(camera.position).normalize();

  for (var value in this.groups) {
    var mesh = this.groups[value].mesh;
    objpos.copy(mesh.position);
    var distance = campos.distanceToSquared(objpos);
    //console.log("Distance: " + distance);
    if (distance < phaseDistance) {
      mesh.visible = true;
      distance -= halfPhaseDistance;
      if (distance < 0) {
        distance = 0;
        mesh.visible = false;
      }
      mesh.material.opacity = distance / maxOpacityPhaseDistance;
      mesh.children[0].visible = true; //CWT NOTE: if switching to cubes instead of particles for hosts, iterate over children for this
      mesh.children[0].material.opacity = 1 - mesh.material.opacity;
    } else {
      mesh.material.opacity = 0.8;
      mesh.children[0].visible = false;
    }
  }
  // console.log("cam: " + camera.position.x + ", " + camera.position.y + ", " + camera.position.z);
}

//-----------------
// Event Listeners
//-----------------

VISDOM.onDocumentMouseMove = function(event) {
  event.preventDefault();

  this.mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
  this.mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;

  //TODO:move tooltip to angular
  this.hud.updateTooltip();
}

VISDOM.onDocumentMouseDown = function(event) {
  event.preventDefault();
  if (this.INTERSECTED !== undefined) {
    //only needed if trying to drag stuff
    //controls.enabled = false;
    this.SELECTED = this.INTERSECTED;
  }
}

VISDOM.onDocumentMouseUp = function(event) {
  event.preventDefault();

  //only needed if trying to drag stuff
  //controls.enabled = true;
  if (this.INTERSECTED !== undefined && this.SELECTED !== undefined && this.INTERSECTED === this.SELECTED) {
    this.SELECTED.visdomObj.toggleSelected();
  } else {
    this.SELECTED = undefined;
  }
}

VISDOM.onWindowResize = function() {

  VISDOM.camera.aspect = window.innerWidth / window.innerHeight;
  VISDOM.camera.updateProjectionMatrix();

  this.renderer.setSize(window.innerWidth, window.innerHeight);
  this.stats.domElement.style.left = (window.innerWidth - 80) + 'px';
}
//--------------------
// Controls init and setup
//--------------------

VISDOM.setControls = function() {
  var camera = this.camera;
  var oldpos = camera.position.clone();
  if (this.params.movement.controls === 'Trackball') {
    this.controls = new THREE.TrackballControls(camera, this.renderer.domElement);
    this.controls.rotateSpeed = 1.0;
    this.controls.zoomSpeed = this.params.movement.trackballZoomSpeed;
    this.controls.panSpeed = 0.8;

    this.controls.noZoom = false;
    this.controls.noPan = false;

    this.controls.staticMoving = false;
    this.controls.dynamicDampingFactor = 0.3;

    this.controls.keys = [65, 83, 68];
  } else if (this.params.movement.controls === 'FlyControl') {
    this.controls = new THREE.FlyControls(camera, this.renderer.domElement);
    this.controls.rollSpeed = this.params.movement.flyRollSpeed;
    this.controls.dragToLook = true;
    this.controls.movementSpeed = this.params.movement.flySpeed;
    this.controls._state = -1;
  } else {
    this.controls = new THREE.FirstPersonControls(camera, this.renderer.domElement);
    this.controls.activeLook = false;
    this.controls.movementSpeed = 20.0;
    this.controls.lookSpeed = 0.05;
  }
  camera.position.copy(oldpos);
  var v0 = new THREE.Vector3(0, 0, 0);
  camera.lookAt(v0);
}

//-----------------
//END THREEJS funcs
//-----------------
//-----------------
// RENDER LOOP
//-----------------

VISDOM.animate = function() {
  var x;
  requestAnimationFrame(this.animate.bind(this), this.renderer.domElement);
  //setTimeout(animate, 17);
  var delta = this.clock.getDelta();
  this.stats.begin();

  this.controls.update(delta);

  this.hud.update();
  TWEEN.update();
  this.updateFlames();
  this.updateDBObjs(this.entities, this.groups);

  this.findIntersections(); //NOTE: if performance issue with this, move to events (should be fine though)
  this.renderer.render(this.scene, this.camera);
  this.stats.end();
}