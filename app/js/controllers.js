/*====================================================================
Copyright 2013 Southfork Security, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
====================================================================*/
visdomNG.controller('versionCtrl', ['$scope', '$timeout',
	function($scope, $timeout) {

	}
]);

visdomNG.controller('navCtrl', ['$scope', '$dialog', '$location', 'navService', 'notify',
	function($scope, $dialog, $location, navService, notify) {
		$scope.navItems = navService.$get();
		$scope.doNavClick = function(nav) {
			nav.click($scope);
		};
		$scope.doStaticNav = function(path) {
			$location.path(path);
		};

		$scope.notifications = notify.$get();
	}
]);

visdomNG.controller('notificationCtrl', ['$scope', 'notify', 'whitelist',
	function($scope, notify, whitelist) {
		//TODO: move this to a modal like flow filters. NOTE: probably make it so it fades in from the top and doesn't disappear when clicking in background
		$scope.notifications = notify.$get();
		$scope.ack = function(notification) {
			notify.acknowledge(notification);
		};
		$scope.fingerprint = function() {
			whitelist.fingerprint();
		};
		$scope.ackAll = function() {
			notify.acknowledgeAll();
		}
	}
]);