/*====================================================================
Copyright 2013 Southfork Security, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
====================================================================*/
visdomNG.factory('socketFlow', ['$rootScope', '$log', '$timeout', '$q', 'notify',
  function($scope, $log, $timeout, $q, notify) {
    'use strict';
    //TODO: separate out socketFlow into websocket service that updates the global flows&ents model,
    //    a flow service that provides the flowArr after filters and sorting, and an ent service that does the same for ends
    var socket = null;
    var flowSocket = {};
    var flows = {}; //e.g. IPv4: channel (e.g. 10.0.0.1:22 <-> 10.0.0.10:5555)
    var entities = {}; //e.g. IPv4: host (e.g. 10.0.0.1) //NOTE: entities in visdom may be a slightly different concept
    var flowFilterObj = {}; //filter object for determining which flows/entities make it into the (optionally sorted) arrays
    var flowKeys = {}; //object map of filterable properties for comparison against flows. used by anyone interested in creating filters
    var filters = []; //array of filterOps for filtering flows from flows into flowArr
    var orderby; //orderOp for prioritizing sorting the flows

    var flowArr = []; //sorted and filtered array. this is the main access to the data
    var entArr = [];

    var loc = window.location,
      new_uri;

    var sendByteDiff = 0;
    var recvByteDiff = 0;

    //smoothie pieces
    var smoothieObj = null;
    var timeSeries = null;
    var updateSeries = false;

    //track promise objects between us and goflow
    var socketDefers = {};

    if (loc.protocol === "https:") {
      new_uri = "wss:";
    } else {
      new_uri = "ws:";
    }
    new_uri += "//" + loc.host;
    new_uri += "/socket";

    flowSocket.url = new_uri;

    var genTimeSeries = function() {
      return {
        totalSend: new TimeSeries({
          resetBoundsInterval: 10000
        }),
        totalRecv: new TimeSeries({
          resetBoundsInterval: 10000
        })
      };
    };

    flowSocket.connect = function(url) {
      flowSocket.url = url || flowSocket.url;
      socket = new WebSocket(flowSocket.url);

      socket.onopen = function() {
        //$log.log("Socket is ready");
        flowSocket.startUpdate();
      };

      socket.onmessage = function(event) {
        var id = 0;
        var msg = JSON.parse(event.data);
        //TODO: change this to allow registration of callbacks so you can do: flowSocket.on("geoipResponse", function(){});
        switch (msg.type) {
          case "flowUpdate":
            flowSocket.updateData(msg.data);
            $scope.$apply();
            break;
          case "geoipResponse":
          case "entityResponse":
            flowSocket.handleDeferResponse(msg.id, msg.data);
            break;
        }
      };
      socket.onclose = function(event) {
        // $log.warn("Websocket to Goflow closed. Attempting to reconnect in 5s");
        $log.warn("The connection to Mimir backend has been interrupted. This may be to due to an update of the Alpha Demo. Please refresh your browser window.");


        var notification = {
          type: "websocket",
          message: "The connection to Mimir backend has been interrupted. This may be to due to an update of the Alpha Demo. Please refresh your browser window."
        };
        notify.add(notification);
        //ALPHA temporarily remove reconnection attempts for alpha version
        // $timeout(flowSocket.connect, 5000);
      };
    };
    flowSocket.startUpdate = function(parms) {
      if (socket.readyState === 1) {
        socket.send("START_FLOW_UPDATE");
      }
    };
    flowSocket.setFrequency = function(rate) {
      socket.send("SET_UPDATE_INTERVAL " + rate);
    };
    flowSocket.$get = function() {
      return flows;
    };
    flowSocket.$getEnts = function() {
      return entities;
    };
    flowSocket.getFlows = function() {
      return flowArr;
    };
    flowSocket.getFlowKeys = function() {
      return flowKeys;
    };
    flowSocket.getEntities = function() {
      return entArr;
    };
    flowSocket.getFilters = function() {
      return filters;
    };
    flowSocket.getOrderby = function() {
      return orderby;
    };
    flowSocket.handleDeferResponse = function(id, data) {
      var defer = socketDefers[id];
      if (defer === undefined) {
        //so you're saying there's a chance.... 
        //wait a minute, what was all that one-in-a-million talk?
        $log.log("handleDeferResponse: Defer unknown response or defer id collision");
        return undefined;
      }
      if (data.length === 1) {
        defer.resolve(data[0]);
        // console.log("*****Response handled:");
        // console.log(data[0]);
      } else if (data.length === 0) {
        defer.resolve({});
      } else {
        defer.reject("Unknown response from goflow");
        return false;
      }
      //this seems to work in testing? not sure if deleting it here is cool or not.
      //  promises.then() seem to be firing fine. I hope GC picks that up later and it's not a leak...
      delete socketDefers[id];
      return true;
    };

    flowSocket.queryGeoIP = function(ip) {
      //goflow protocol needs a random id attached to a message so when we're doing
      //  onmessage in the websocket handler we know which promise to resolve
      var id = Math.floor(Math.random() * 999999999).toString();
      if (socket.readyState !== 1) {
        return undefined;
      }
      socketDefers[id] = $q.defer();
      socket.send("GET_GEO_LOCATION " + id + " " + ip);

      return socketDefers[id].promise;
    };

    flowSocket.queryEntity = function(entid) {
      //----------------
      //NOTE: temporarily static until backend supports entity resolution
      //TODO: support entity resolution in backend
      var id = Math.floor(Math.random() * 999999999).toString();

      socketDefers[id] = $q.defer();
      var ret = [{
          'id': entid,
          'name': 'ip ' + entid,
          'fromMimir': false,
        }
      ];
      Object.defineProperty(ret[0], "fromMimir", {
        enumerable: false
      });

      $timeout(function() {
        flowSocket.handleDeferResponse(id, ret);
      }, 5);
      return socketDefers[id].promise;


      //---------------- END TMP stuff

      //goflow protocol needs a random id attached to a message so when we're doing
      //  onmessage in the websocket handler we know which promise to resolve
      /*var id = Math.floor(Math.random() * 999999999).toString();
      if (socket.readyState !== 1) {
        return undefined;
      }
      socketDefers[id] = $q.defer();
      socket.send("GET_ENTITY " + id + " " + entid);
      return socketDefers[id].promise;*/
    };

    function resolveEnt(flow, entobj) {
      var entid = entobj.ip;
      //initialize base entity
      entities[entid] = {
        'id': entid,
        'name': entid,
        flows: []
      };
      Object.defineProperty(entities[entid], "flows", {
        enumerable: false
      });
      entities[entid].flows.push(flow);
      // console.log('entity unknown: ' + entid);
      entobj.ent = entities[entid];

      return $q.all([flowSocket.queryGeoIP(entid), flowSocket.queryEntity(entid)]).then(function(result) {
        angular.extend(entities[entid], result[1]);
        entities[entid].location = result[0].location;
        //remove enumerable on location so it doesn't show up in filter. //TODO add filtering on location
        Object.defineProperty(entities[entid], "location", {
          enumerable: false
        });
        if (entities[entid].location !== undefined) {
          entities[entid].location.lat = Number(entities[entid].location.lat);
          entities[entid].location.long = Number(entities[entid].location.long);
        }
        dispatch('entityCreate', entid);
        return result;
      }, function(err) {
        console.log(err);
        return err;
      });
    };

    flowSocket.fillEntityInfo = function(flowid) {
      //TODO: move flow specific entity info (such as port) to the overall flow object so that flow.src can just point at the entitiy directly. flow.src.ent is dumb
      var flow = flows[flowid];
      var src, dst;
      var promises = [];
      var fireEvent = false;

      if (entities[flow.src.ip] === undefined) {
        promises.push(resolveEnt(flow, flow.src));
        // console.log("new entity: " + flow.src.ip);
      } else {
        flow.src.ent = entities[flow.src.ip];
        entities[flow.src.ip].flows.push(flow);
      }
      if (entities[flow.dst.ip] === undefined) {
        // console.log("new entity: " + flow.dst.ip);
        promises.push(resolveEnt(flow, flow.dst));
      } else {
        flow.dst.ent = entities[flow.dst.ip];
        entities[flow.dst.ip].flows.push(flow);
      }
      $q.all(promises).then(function(result) {
        // console.log("ready to spark");
        dispatch('flowCreate', flowid);
      }, function(err) {
        console.error(err);
      });
    };
    flowSocket.destroyEntity = function(entid) {
      //TODO: allow callback registration on the flowCreate/Update/Destroy and entityCreate/Update/Destroy so that flowSocket can be the one to take care of deleting shit
      dispatch('entityDestroy', entid);
      delete entities[entid];
    }

    flowSocket.destroyFlow = function(flowid) {
      //TODO: dispatch an event on this so anyone monitoring flows can adjust
      //TODO: dispatch entity delete event
      var flow = flows[flowid];
      var e1 = flow.src.ent;
      var e2 = flow.dst.ent;
      e1.flows.splice(e1.flows.indexOf(flow), 1);
      if (e1.flows.length === 0) {
        flowSocket.destroyEntity(e1.id);
      }
      e2.flows.splice(e2.flows.indexOf(flow), 1);
      if (e2.flows.length === 0) {
        flowSocket.destroyEntity(e2.id);
      }
      //TODO: allow callback registration on the flowCreate/Update/Destroy and entityCreate/Update/Destroy so that flowSocket can be the one to take care of deleting shit
      delete flows[flowid];
      $scope.$broadcast('flowDestroy', flowid);
    };

    var dispatch = function(type, obj) {
      $scope.$broadcast(type, obj);
    };

    flowSocket.updateData = function(newdata) {
      var sendByteDiff = 0,
        recvByteDiff = 0,
        itemcount = 0;
      newdata.forEach(function(element, index, array) {
        var flowChanged = false;
        if (flows.hasOwnProperty(element.flowID)) {
          //update stuff
          //TODO: move this logic to getter and setters on the object that store the diff bytes in another property
          var newsend = Math.floor(parseInt(element.info.sendbytes, 10) - flows[element.flowID].sendbytes);
          var newrecv = Math.floor(parseInt(element.info.recvbytes, 10) - flows[element.flowID].recvbytes);
          if (newsend > 0) {
            sendByteDiff += newsend;
            flows[element.flowID].totalbytes += newsend;
            flows[element.flowID].sendbytes = parseInt(element.info.sendbytes, 10);
            flowChanged = true;
          }
          if (newrecv > 0) {
            recvByteDiff += newrecv;
            flows[element.flowID].totalbytes += newrecv;
            flows[element.flowID].recvbytes = parseInt(element.info.recvbytes, 10);
            flowChanged = true;
          }
          flows[element.flowID].isCurrent = true;
        } else {
          //it's a new flow!
          flows[element.flowID] = element.info;
          flows[element.flowID].id = Number(element.flowID);
          flows[element.flowID].sendbytes = parseInt(element.info.sendbytes, 10);
          flows[element.flowID].recvbytes = parseInt(element.info.recvbytes, 10);
          flows[element.flowID].src.port = parseInt(element.info.src.port, 10);
          flows[element.flowID].dst.port = parseInt(element.info.dst.port, 10);
          flows[element.flowID].totalbytes = flows[element.flowID].sendbytes + flows[element.flowID].recvbytes;
          sendByteDiff += flows[element.flowID].sendbytes;
          recvByteDiff += flows[element.flowID].recvbytes;
          flowChanged = false;
          flows[element.flowID].isCurrent = true;

          //fillEntityInfo fires the create flow event
          flowSocket.fillEntityInfo(element.flowID); //async fill in flow info (rDNS, geoIP, etc)
        }
        if (flowChanged === true) {
          //NOTE: if we see the flow again before the geolocation has a chance to come back on the entities
          //  this can get called to fire a spark before we know where the entity is.
          dispatch('flowUpdate', element.flowID);
        }
        itemcount++;
      });
      if (updateSeries === true) { //update time series if there is a graph
        //TODO: make this dynamic so users can specify which flows to graph on smoothie
        timeSeries.totalSend.append(new Date().getTime(), sendByteDiff);
        timeSeries.totalRecv.append(new Date().getTime(), recvByteDiff);
      }

      //Go back through and remove expired flows (don't know a better way to do this...)
      Object.keys(flows).forEach(function(element, index, array) {
        if (flows[element].hasOwnProperty("isCurrent")) {
          delete flows[element].isCurrent;
        } else {
          flowSocket.destroyFlow(element);;
        }
      });
      //TODO: Move this to a better place. Do it as you're analyzing the flow not afterward. Filtering out afterward is slightly less efficient but who cares?
      //for now  use the first flow to generate the flow keys object. flows can have different properties though depending on protocol so TODO: make more agnostic
      for (var prop in flowKeys) {
        delete flowKeys[prop];
      }

      flowSocket.generateKeysMap(flows[Object.keys(flows)[0]], flowKeys);
      flowSocket.generateKeysMap(flows[Object.keys(flows)[1]], flowKeys);

      flowSocket.applyFilter(flows, filters, flowArr);
    };


    //########### sorting and filter helpers ##########
    flowSocket.evalPath = function(path, obj) {
      function index(obj, i) {
        if (obj === undefined) {
          return undefined;
        }
        return obj[i];
      }
      return path.split('.').reduce(index, obj);
    };

    //########### sorting #########
    flowSocket.orderOp = function(path, asc) {
      this.asc = asc;
      this.path = path;
    };
    flowSocket.orderOp.prototype.compare = function(v1, v2) {
      var p1 = flowSocket.evalPath(this.path, v1);
      var p2 = flowSocket.evalPath(this.path, v2);
      if (p1 === undefined || p2 === undefined) {
        return undefined;
      }
      // console.log("comparing " + this.path + ". p1 = " + p1 + ". p2 = " + p2);
      if (p1 == p2) {
        return 0;
      }
      if (this.asc === true) {
        if (p1 < p2) {
          return -1;
        } else {
          return 1;
        }
      } else {
        if (p1 < p2) {
          return 1;
        } else {
          return -1;
        }
      }
    };
    flowSocket.binaryInsert = function(objarray, findObj, orderOp) {
      var low = 0,
        high = objarray.length - 1,
        i, comparison;
      if (objarray.length === 0 || orderOp === undefined) {
        return objarray.push(findObj);
      }
      while (low <= high) {
        i = Math.floor((low + high) / 2);
        comparison = orderOp.compare(objarray[i], findObj);
        if (comparison === undefined) {
          console.error("binaryInsert error. oderOp.path not found in the object array");
          return undefined;
        }
        if (comparison < 0) {
          low = i + 1;
          continue;
        };
        if (comparison > 0) {
          high = i - 1;
          continue;
        };
        break;
      }
      comparison = orderOp.compare(objarray[low], findObj);
      if (comparison <= 0) {
        low += 1;
      }
      objarray.splice(low, 0, findObj);
      return i;
    };
    flowSocket.setOrderby = function(path, asc) {
      if (asc === undefined) {
        asc = true;
      }
      orderby.asc = asc;
      orderby.path = path || "id";
    };
    flowSocket.toggleOrderby = function() {
      orderby.asc = !orderby.asc;
    }

    //########### filters ###########
    // flowSocket wil keep an overall total objectmap of flows but gives out the filtered/sorted array of flows
    //  to users of the service
    flowSocket.generateKeysMap = function(obj, sumobj) {
      for (var prop in obj) {
        if (typeof obj[prop] === "object" && Array.isArray(obj[prop]) === false) {
          sumobj[prop] = {};
          flowSocket.generateKeysMap(obj[prop], sumobj[prop]);
        } else {
          sumobj[prop] = true; //set to enable filtering on this key by default
        }
      }
    };

    flowSocket.filterOp = function(op, val, path, logicalOr) {
      //TODO: support multiple filters (eg < 100 and > 50)
      this.op = op;
      this.val = val;
      this.path = path;
      if (logicalOr) {
        this.logicalOr = true;
      } else {
        this.logicalOr = false;
      }
    };
    flowSocket.filterOp.prototype.test = function(obj) {
      //TODO: improve logic here for strings and all that jazz. is it possible to make this agnostic or too much a PITA?
      //NOTE: the comparator returns whether or not a thing passes the filter
      //  e.g. If the desired op is "< 100", 75 will return true to indicate passing the filter

      var prop = flowSocket.evalPath(this.path, obj);
      if (prop === undefined) {
        console.error("Filter+flowobject mismatch problem. Reset filters");
        return undefined;
      }
      // var prop = obj;
      switch (this.op) {
        case "<":
          return (prop < this.val);
          break;
        case ">":
          return (prop > this.val);
          break;
        case "=":
          if (typeof prop === "string") {
            return (prop.indexOf(this.val) !== -1);
          } else {
            return (prop == this.val);
          }
          break;
        case "!":
          if (typeof prop === "string") {
            return (prop.indexOf(this.val) === -1);
          } else {
            return (prop != this.val);
          }
          break;
      }
    };

    flowSocket.addFilter = function(filter) {
      if (filter !== undefined) {
        filters.push(filter);
      }
    };
    flowSocket.removeFilter = function(index) {
      filters.splice(index, 1);
    };
    flowSocket.clearFilters = function() {
      filters.length = 0;
    };

    flowSocket.checkFilter = function(obj, filterlist) { //does the object pass through the filterlist?
      //when using arrays of filterOps (filter groups, e.g. sendbytes < 100 and sendbytes > 100)
      //   filterlist should look like: [{filterOps: [<array of filterOps>] <, logicalOr: boolean>}]
      var retval = true;
      var hadAnAndFilter = false;
      var orval = false;
      if (filterlist.length === 0) {
        return true;
      }
      filterlist.forEach(function(filter, ind, arr) {
        if (!(filter instanceof flowSocket.filterOp)) {
          if (filter.logicalOr && !orval) {
            orval = flowSocket.checkFilter(obj, filter.filterOps);
          } else if (retval) {
            hadAnAndFilter = true;
            retval = flowSocket.checkFilter(obj, filter.filterOps);
          }
        } else {
          if (filter.logicalOr && !orval) {
            orval = filter.test(obj);
          } else if (retval) {
            hadAnAndFilter = true;
            retval = filter.test(obj);
          }
        }
      });
      //return the sum of all AND filters (but not if we never saw one, otherwise only or filters match everything) or the OR filters
      return (retval && hadAnAndFilter) || orval;
    };

    //call when a new filter goes active otherwise updateData should continue to operate off of the current filter
    flowSocket.applyFilter = function(objmap, filterlist, finalarray) {
      // console.log(filterobj);
      finalarray = finalarray || []; //reset finalarray. create new if not provided
      finalarray.length = 0;

      Object.keys(objmap).forEach(function(val, ind, arr) {
        if (flowSocket.checkFilter(objmap[val], filterlist)) {
          flowSocket.binaryInsert(finalarray, objmap[val], orderby);
        }
      });
      return finalarray;
    };

    //################## end filters ##############

    flowSocket.toggleTimeSeries = function() {
      updateSeries = !updateSeries;
      timeSeries = genTimeSeries();
    };
    flowSocket.getTimeSeries = function() {
      return timeSeries;
    };

    //############## INIT code ###########
    orderby = new flowSocket.orderOp("id", true); //order by id default
    timeSeries = genTimeSeries();
    flowSocket.connect();
    return flowSocket;
  }
]);