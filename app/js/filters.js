/*====================================================================
Copyright 2013 Southfork Security, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
====================================================================*/
/* Filters */

visdomNG.
filter('interpolate', ['version', function(version) {
	'use strict';
	return function(text) {
		return String(text).replace(/\%VERSION\%/mg, version);
	};
}]);

visdomNG.
filter('bytesHuman', function() {
	'use strict';
	return function(bytecount) {
		var i = -1;
		var byteUnits = [' kB', ' MB', ' GB', ' TB', 'PB', 'EB', 'ZB', 'YB'];
		do {
			bytecount = bytecount / 1024;
			i++;
		} while (bytecount > 1024);

		return Math.max(bytecount, 0.1).toFixed(1) + byteUnits[i];
	};
});