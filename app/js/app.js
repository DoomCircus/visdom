/*====================================================================
Copyright 2013 Southfork Security, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
====================================================================*/
// Declare app level module which depends on modules installed to add routeProvider listeners and the like
// Dependency injection loads modules as they are referenced.
//ui.bootstrap is angular version for bootstrap components
var visdomNG = angular.module('visdomNG', ['visdomNG.flowView', 'visdomNG.entView', 'visdomNG.vis3d', 'ui.bootstrap']).
config(['$routeProvider',
	function($routeProvider) {
		'use strict';
		$routeProvider.when('/version', {
			templateUrl: 'partials/version.html',
			controller: 'versionCtrl'
		});
		$routeProvider.when('/notifications', {
			templateUrl: 'partials/notifications.html',
			controller: 'notificationCtrl'
		});
		$routeProvider.otherwise({
			redirectTo: '/version'
		});
	}
]);

visdomNG.run(['whitelist',
	function(whitelist) {
		//let's kick off the whitelisting service immediately so it will listen for blacklists and such
	}
]);