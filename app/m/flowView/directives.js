/*====================================================================
Copyright 2013 Southfork Security, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
====================================================================*/
/* Directives */

visdomNG.flowView.
directive('flowSmoothie', function() {
	'use strict';
	return {
		// scope: {
		// 	"chart": "="
		// },
		link: function(scope, elm, attrs) {
			var timeSeries = scope.chart; //chart has to be set by controller (or ng-repeat directive, etc)

			var chart = new SmoothieChart({ //NOTE library requirement
				grid: {
					strokeStyle: 'rgba(43, 43, 214, 0.7)',
					fillStyle: 'rgba(43, 43, 214, 0.2)',
					lineWidth: 1,
					millisPerLine: 250,
					verticalSections: 6
				},
				labels: {
					fillStyle: 'rgb(255, 255, 255)'
				}
			});
			timeSeries.forEach(function(element, index, array) {
				chart.addTimeSeries(element, {
					strokeStyle: 'rgba(' + (43 + (60 * index)) + ', 43, ' + (190 + (15 * index)) + ', 1)',
					fillStyle: 'rgba(' + (43 + (60 * index)) + ', 43, ' + (190 + (15 * index)) + ', 0.3)',
					lineWidth: 3
				});
			});

			chart.streamTo(elm[0], 1000);
		}
	};
});

visdomNG.flowView.directive('flow', function() {
	return {
		template: '<div class="well" style="margin:2px;padding:9px"><a href="#/flows/{{flow.id}}"><strong>ID: {{flow.id}}</a></strong><br><a href="#/entities/{{flow.src.ent.id}}">{{flow.src.ip}}:{{flow.src.port}}</a> <i class="icon-arrow-right"></i> {{flow.sendbytes | bytesHuman}} <br>{{flow.recvbytes | bytesHuman}} <i class="icon-arrow-left"></i> <a href="#/entities/{{flow.dst.ent.id}}">{{flow.dst.ip}}:{{flow.dst.port}}</a></div>'
	}
});

visdomNG.flowView.directive('filterPrinter', function() {
	//TODO: use <ul> and an <li> for each subfilter group recursively to display filters
	function buildHtml(filter) {
		var string = "";
		if (filter.filterOps !== undefined) {
			if (filter.logicalOr) {
				string += "OR";
			} else {
				string += "AND";
			}
			string += "<ul>";
			filter.filterOps.forEach(function(val, ind, arr) {
				string += "<li>" + buildHtml(val) + "</li>";
			});
			string += "</ul>"
		} else {
			string += (filter.logicalOr && "OR" || "AND") + " " + filter.path + " " + filter.op + " " + filter.val;
		}
		return string;
	}
	return {
		restrict: "E",
		link: function(scope, elm, attrs) {
			elm.html(buildHtml(scope.filter));
		}
	}
});