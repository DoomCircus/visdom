/*====================================================================
Copyright 2013 Southfork Security, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
====================================================================*/
// Declare module which depends on filters, and services
visdomNG.flowView = angular.module('visdomNG.flowView', ['visdomNG', 'ngResource', 'ui.bootstrap']).
config(['$routeProvider',
	function($routeProvider) {
		$routeProvider.when('/flowgraph', {
			templateUrl: 'm/flowView/partials/flowGraph.html',
			controller: 'flowGraphCtrl'
		});
		$routeProvider.when('/flowview', {
			templateUrl: 'm/flowView/partials/flowView.html',
			controller: 'flowViewCtrl'
		});
		$routeProvider.when('/flows/:flowID', {
			templateUrl: 'm/flowView/partials/flowDetail.html',
			controller: 'flowDetailCtrl'
		});
	}
]).run(['navService', '$dialog', '$location',
	function(navService, $dialog, $location) {
		navService.register({
			"text": "Flow graphs",
			click: function() {
				$location.path("/flowgraph");
			}
		});
		navService.register({
			"text": "Flows list",
			click: function() {
				$location.path("/flowview");
			}
		});
		//registir with Core Nav
		var flowFilter = {};
		flowFilter.opts = {
			backdrop: true,
			keyboard: true,
			backdropClick: true,
			backdropFade: true,
			templateUrl: 'm/flowView/partials/flowFilter.html',
			controller: 'flowFilterCtrl'
		};
		flowFilter.text = "Flow Filters";
		flowFilter.click = function() {
			var d = $dialog.dialog(this.opts);
			d.open();
		}
		navService.register(flowFilter);
	}
]);