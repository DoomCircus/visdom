/*====================================================================
Copyright 2013 Southfork Security, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
====================================================================*/
visdomNG.flowView.factory('commonFilters', ['$http', 'socketFlow',
	function($http, socketFlow) {
		var categories = ["Network Address", "Ports", "Traffic Rate"]; //NOTE: these coincide with index into the array of customFilters.json
		var filterarray = [];
		$http.get('m/flowView/sampleFlowFilters.json').success(function(data) {
			angular.copy(data, filterarray);
		});

		function parseFilter(filter, op) {
			var value, retOps;
			if (op.filterOps !== undefined) {
				retOps = {
					"logicalOr": op.logicalOr,
					filterOps: []
				};
				op.filterOps.forEach(function(subOp, ind, arr) {
					retOps.filterOps.push(parseFilter(filter, subOp));
				});
			} else {
				if (filter.modifiable) {
					if (!isNaN(filter.userVal)) {
						//let's guess if they meant a number. sometimes loose typing sucks. we wouldn't want to have "100" < "30" to be true.
						value = Number(filter.userVal);
					} else {
						value = filter.userVal;
					}
				} else {
					value = op.val;
				}
				retOps = new socketFlow.filterOp(op.op, value, op.path, op.logicalOr);
			}
			return retOps;
		}

		return {
			getCategories: function() {
				return categories;
			},
			getFilters: function() {
				return filterarray;
			},
			apply: function(filter) {
				//convert from JSON into usable filter for the socketFlow service
				filter.filterOps.forEach(function(op, ind, arr) {
					socketFlow.addFilter(parseFilter(filter, op));
				});
			}
		}
	}
]);