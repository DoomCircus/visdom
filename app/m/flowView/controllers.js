/*====================================================================
Copyright 2013 Southfork Security, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
====================================================================*/
/* Controllers */
visdomNG.flowView.controller('flowGraphCtrl', ['$scope', 'socketFlow',
	function($scope, socketFlow) {
		'use strict';
		$scope.flows = socketFlow.getFlows();
		$scope.getFlowCount = function() {
			return $scope.flows.length;
		}

		//set up smoothie charts
		socketFlow.toggleTimeSeries();
		var series = socketFlow.getTimeSeries();

		//charts array for smoothies. these are initially pushed for testing/default
		$scope.charts = [];
		var mychart = [series.totalSend, series.totalRecv];
		mychart.title = "All traffic";
		mychart.desc = "Purple:received, Blue:sent";
		// $scope.charts.push([series.averageSend, series.averageRecv]);
		$scope.charts.push(mychart);

		$scope.$on('$destroy', function() {
			socketFlow.toggleTimeSeries();
		});
	}
]);

visdomNG.flowView.controller('flowDetailCtrl', ['$scope', '$log', 'socketFlow', '$routeParams', '$timeout',
	function($scope, $log, socketFlow, $routeParams, $timeout) {
		"use strict";
		$scope.flowID = $routeParams.flowID;
		var flows = socketFlow.$get();
		var graphWatcher;

		$scope.flow = flows[$scope.flowID];
		$scope.$emit('flowFocused', $scope.flowID); //for non-angular interface listeners. vis3d

		//needed because socketFlow has to start up in the background. TODO: move to more RESTFUL grabbing of individual flows and entities. use promises
		var watcher = $scope.$watch(function() {
			return flows[$scope.flowID]
		}, function() {
			$scope.flow = flows[$scope.flowID];
			if ($scope.flow !== undefined) {
				//flow graph
				graphTimer = $timeout(updateChart, 300);
				watcher(); //unregisterz
			}
		});

		//######## flow graph ##########
		//TODO: move dynamically into the service
		var sendSeries = new TimeSeries({
			resetBoundsInterval: 10000
		});
		var recvSeries = new TimeSeries({
			resetBoundsInterval: 10000
		});
		$scope.chart = [sendSeries, recvSeries];

		var graphTimer;
		var oldSend, oldRecv;

		function updateChart() {
			sendSeries.append(new Date().getTime(), $scope.flow.sendbytes - oldSend);
			recvSeries.append(new Date().getTime(), $scope.flow.recvbytes - oldRecv);
			oldSend = $scope.flow.sendbytes;
			oldRecv = $scope.flow.recvbytes;
			graphTimer = $timeout(updateChart, 300);
		}

		$scope.$on('destroy', function() {
			$timeout.cancel(graphTimer);
		});
		//###########

		//######### whitelisting ########
		$scope.dolist = function(flow, type) {
			flow.list = type;
		};
	}
]);

//NOTE: The dialog service is injected by $dialog from ui-bootstrap
//	this controller should not be instantiated on it's own

visdomNG.flowView.controller('flowFilterCtrl', ['$scope', 'socketFlow', 'dialog', 'commonFilters',
	function($scope, socketFlow, dialog, commonFilters) {
		$scope.close = function() {
			dialog.close();
		};

		//############ common ipv4 filter picking ##########

		$scope.categories = commonFilters.getCategories();
		$scope.sampleFilters = commonFilters.getFilters();


		$scope.applySampleFilter = function(sampleFilter) {
			commonFilters.apply(sampleFilter);
			$scope.resetFilter();
			$scope.showFilterOptions = false;
		};

		//############ FILTERS ############
		var filterset = false;
		$scope.operators = ["<", ">", "=", "!"];

		$scope.getKeys = function(obj) {
			var keys = Object.keys(obj);
			for (var i = keys.length - 1; i >= 0; i--) {
				if (keys[i][0] === "$") {
					keys.splice(i, 1);
				}
			}
			return keys;
		}
		$scope.updatePropSelector = function() {
			if ($scope.currentPath === "") {
				$scope.currentPath += $scope.currentKey;
			} else {
				$scope.currentPath = $scope.currentPath + "." + $scope.currentKey;
			}
			if (typeof $scope.currentObj[$scope.currentKey] === "object") {
				$scope.currentObj = $scope.currentObj[$scope.currentKey];
			} else {
				$scope.pickOp = true;
			}
			$scope.buildStarted = true;
		};

		$scope.createCustomFilter = function() {
			if ($scope.currentOp === "" || $scope.currentPath === "" || $scope.currentCompVal === "") {
				$scope.filterProblem = true;
				return;
			}
			var compval;
			if (!isNaN($scope.currentCompVal)) {
				//let's guess if they meant a number. sometimes loose typing sucks. we wouldn't want to have "100" < "30" to be true.
				compval = Number($scope.currentCompVal);
			} else {
				compval = $scope.currentCompVal;
			}

			socketFlow.addFilter(new socketFlow.filterOp($scope.currentOp, compval, $scope.currentPath, $scope.filterLogicalOr));
			$scope.resetFilter();
		};
		$scope.removeFilter = function(filterindex) {
			socketFlow.removeFilter($scope.filters.indexOf(filterindex));
		};
		$scope.clearAlert = function() {
			$scope.filterProblem = false;
		}
		$scope.resetFilter = function() {
			$scope.currentObj = $scope.flowKeys;
			$scope.currentPath = "";
			$scope.pickOp = false;
			$scope.currentKey = "";
			$scope.currentOp = "";
			$scope.currentCompVal = "";
			$scope.buildStarted = false;
			$scope.filterProblem = false;
		};
		$scope.resetFilter();

		$scope.filters = socketFlow.getFilters();
		$scope.flowKeys = socketFlow.getFlowKeys(); //from generateKeysMap in updateData
		$scope.currentObj = $scope.flowKeys;
	}
])

visdomNG.flowView.controller('flowViewCtrl', ['$scope', 'socketFlow', '$location',
	function($scope, socketFlow, $location) {
		"use strict";

		//########### Filters #############
		$scope.removeFilter = function(filterindex) {
			socketFlow.removeFilter($scope.filters.indexOf(filterindex));
		};
		$scope.filters = socketFlow.getFilters();
		$scope.flows = socketFlow.getFlows();


		//############# orderby #############
		$scope.flowKeys = socketFlow.getFlowKeys(); //from generateKeysMap in updateData

		$scope.getKeys = function(obj) {
			var keys = Object.keys(obj);
			for (var i = keys.length - 1; i >= 0; i--) {
				if (keys[i][0] === "$") {
					keys.splice(i, 1);
				}
			}
			return keys;
		}

		//TODO: reduce the reuse of code between orderby and custom filters in this directive
		$scope.resetOrderby = function() {
			$scope.orderbyObj = $scope.flowKeys;
			$scope.orderbyPath = "";
			$scope.pickOrderbyOp = false;
			$scope.orderbyKey = "";
			$scope.orderbyAsc = true;
			$scope.orderbyStarted = false;
			$scope.chooseOrder = false;
		}
		$scope.updateOrderSelector = function() {
			if ($scope.orderbyPath === "") {
				$scope.orderbyPath += $scope.orderbyKey;
			} else {
				$scope.orderbyPath = $scope.orderbyPath + "." + $scope.orderbyKey;
			}
			if (typeof $scope.orderbyObj[$scope.orderbyKey] === "object") {
				$scope.orderbyObj = $scope.orderbyObj[$scope.orderbyKey];
			} else {
				$scope.pickOrderbyOp = true;
			}
			$scope.orderbyStarted = true;
		};
		$scope.createOrderby = function() {
			socketFlow.setOrderby($scope.orderbyPath, $scope.orderbyAsc);
			$scope.resetOrderby();
		};
		$scope.toggleOrderby = function() {
			socketFlow.toggleOrderby();
		}
		$scope.changeOrder = function() {
			$scope.chooseOrder = true;
		};
		$scope.chooseOrder = false;
		$scope.resetOrderby();
		$scope.orderby = socketFlow.getOrderby();

		//######## data #########
		$scope.flowdetail = function($event, id) {
			$event.stopPropagation();
			$location.path("/flows/" + id);
		};
		$scope.entdetail = function($event, id) {
			$event.stopPropagation();
			$location.path("/entities/" + id);
		};

		//############## whitelisting ##########
		$scope.applylistMatching = function(type) {
			$scope.flows.forEach(function(val, ind, array) {
				val.list = type;
			});
		}
	}
]);