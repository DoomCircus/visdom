/*====================================================================
Copyright 2013 Southfork Security, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
====================================================================*/
// Declare module which depends on filters, and services
visdomNG.entView = angular.module('visdomNG.entView', ['visdomNG', 'ui.bootstrap']).
config(['$routeProvider',
	function($routeProvider) {
		//$routeProvider.when('/entities', {templateUrl: 'm/entView/partials/allEnts.html', controller: 'allEntsCtrl'});
		//$routeProvider.when('/entfilter', {templateUrl: 'm/entView/partials/entFilter.html', controller: 'entFilterCtrl'});
		$routeProvider.when('/entities/:entID', {
			templateUrl: 'm/entView/partials/entDetail.html',
			controller: 'entDetailCtrl'
		});
	}
]);

visdomNG.entView.
filter('entLocation', function() {
	'use strict';
	return function(location) {
		if (location === undefined) {
			return "Non-Routable/Private";
		} else {
			return "Lat: " + location.lat + ", Long: " + location.long;
		}
	};
});