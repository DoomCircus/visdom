/*====================================================================
Copyright 2013 Southfork Security, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
====================================================================*/
visdomNG.entView.controller('entDetailCtrl', ['$scope', '$log', 'socketFlow', '$routeParams', '$location',
	function($scope, $log, socketFlow, $routeParams, $location) {
		"use strict";
		$scope.entID = $routeParams.entID;
		var ents = socketFlow.$getEnts();

		//needed because socketFlow has to start up in the background. TODO: move to more RESTFUL grabbing of individual flows and entities
		$scope.entity = ents[$scope.entID];
		$scope.$watch(function() {
			return ents[$scope.entID]
		}, function() {
			$scope.entity = ents[$scope.entID];
		});

		$scope.printLocation = function() {
			if ($scope.entity.location === undefined) {
				return "Non-Routable";
			} else {
				return "Lat: " + $scope.entity.location.lat + ", Long: " + $scope.entity.location.long;
			}
		};
		$scope.flowdetail = function($event, id) {
			$event.stopPropagation();
			$location.path("/flows/" + id);
		};
		$scope.entdetail = function($event, id) {
			$event.stopPropagation();
			$location.path("/entities/" + id);
		};

		$scope.$emit('entityFocused', $scope.entID);

		//######### whitelisting ########
		$scope.dolist = function(ent, type) {
			ent.list = type;
		};
	}
]);