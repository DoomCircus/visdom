/*====================================================================
Copyright 2013 Southfork Security, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
====================================================================*/
//This module is the interface between VISDOM/webGL and angular. Angular is responsible
// for keeping the model up to date with the backend server. VISDOM will view that model
// through these angular hooks and react accordingly to changes. This way angular(socketFlow)
//  doesn't have to know anything about the parties interested in it's data.
visdomNG.vis3d = angular.module('visdomNG.vis3d', []);

visdomNG.vis3d.factory('vis3d', ['$log', '$rootScope', 'socketFlow', '$location',
  function($log, $rootScope, socketFlow, $location) {
    'use strict';
    var vis3d = {};
    // var vis3d = VISDOM;
    vis3d.start = function(attachDom) {
      $log.log("vis3d: initializing VISDOM");
      VISDOM.init(attachDom);
      VISDOM.animate();
    };
    vis3d.setScope = function(scope) {
      VISDOM.$scope = scope;
    };
    vis3d.sayhi = function(attachDom) {
      $log.log("hi from vis3d");
    };

    $rootScope.$on('vis3dNav', function(e, url) {
      console.log("vis3dNav to: " + url);
      $location.path(url);
    });

    vis3d.handleUpdate = function(e, flowID) {
      //TODO: move this stuff to this service and out of VISDOM entirely?
      var flow = socketFlow.$get()[flowID];

      var e1id = flow.src.ent.id + ":" + flow.src.port;
      var e2id = flow.dst.ent.id + ":" + flow.dst.port;
      var g1id, g2id;
      if (flow.src.ent.hasOwnProperty("location")) {
        if (flow.src.ent.location !== undefined) {
          g1id = "GEOLOCATED";
        } else {
          g1id = flow.src.ent.id;
        }
      }
      if (flow.dst.ent.hasOwnProperty("location")) {
        if (flow.dst.ent.location !== undefined) {
          g2id = "GEOLOCATED";
        } else {
          g2id = flow.dst.ent.id;
        }
      }
      //If geolocation hasn't come back yet skip this spark I guess??
      if (g1id !== undefined && g2id !== undefined) {
        //TODO: refactoring to unify visdomObj and angular to be single objects
        var e1 = VISDOM.getEntityByID(e1id, g1id);
        var e2 = VISDOM.getEntityByID(e2id, g2id);
        if (g1id === "GEOLOCATED") {
          e1.group.geolocateEnt(e1, flow.src.ent.location);
        }
        if (g2id === "GEOLOCATED") {
          e2.group.geolocateEnt(e2, flow.dst.ent.location);
        }
        VISDOM.createLineSpark(e1, e2);
      }
      return true;
    };
    return vis3d;
  }
]);

visdomNG.vis3d.controller('vis3dCtrl', ['$scope', 'vis3d',
  function($scope, vis3d) {
    "use strict";
    $scope.$on('flowUpdate', vis3d.handleUpdate);
    $scope.vis3d = vis3d;
    vis3d.setScope($scope);
  }
]);

visdomNG.vis3d.
directive('visdom', function() {
  'use strict';
  return function(scope, elm, attrs) {
    scope.vis3d.start(elm[0]); //directive initializes the threejs environment
  };
});